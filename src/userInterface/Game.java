package userInterface;

import data.Boss;
import data.Bullet;
import data.Deadbody;
import data.Enemy;
import data.Flame;
import data.Hitmarker;
import data.Laser;
import data.Loot;
import data.Weapon;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.effect.Lighting;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.transform.Rotate;
import specifications.ChildViewService;
import tools.HardCodedParameters;

public class Game implements ChildViewService {
	private Viewer v;

	@Override
	public void onBindParentView(Viewer v) {
		this.v = v;
	}

	@Override
	public Group getPanel() {
		Group panel = new Group();
		Rectangle map = new Rectangle(v.shrink * Viewer.defaultMainWidth*1.1,v.shrink * Viewer.defaultMainHeight);
		ImagePattern imgBackground = null;

		switch (v.data.getGame().getMap().getBackground()) {
		case "backgroundMapHell":
			imgBackground = new ImagePattern(v.backgroundMapHell);
			break;
		case "backgroundMapHeaven":
			imgBackground = new ImagePattern(v.backgroundMapHeaven);
			break;
		}

		map.setFill(imgBackground);
		map.setTranslateX(-10);
		map.setTranslateY(0);
		
		Rectangle Weapon = new Rectangle(-2 * v.xModifier + v.shrink * Viewer.weaponWidth,
				-.2 * v.shrink * Viewer.weaponHeight + v.shrink * Viewer.weaponHeight);
		Weapon.setTranslateX(v.shrink * Viewer.defaultMainWidth *0.75);
		Weapon.setTranslateY(v.shrink * Viewer.defaultMainHeight / 1.13);

		double radiusHero = .5 * Math.min(v.shrink * v.data.getHero().getWidth(), v.shrink * v.data.getHero().getHeight());

		// on selectionne l'image du hero
		// si il est entrain de tirer / si il est entrain de tirer et saigner / en
		// fonction de l'arme
		Weapon currentWeapon = v.data.getHero().getCurrentWeapon();
		Text ammo;
		if (currentWeapon.getAmmo() == -1) {
			ammo = new Text(v.shrink * Viewer.defaultMainWidth * 0.99,v.shrink * Viewer.defaultMainHeight / 1.06 , "\u221E");
		} else {
			ammo = new Text(v.shrink * Viewer.defaultMainWidth * 0.99,v.shrink * Viewer.defaultMainHeight / 1.06, "" + currentWeapon.getAmmo());
		}
		ammo.setFont(new Font(.05 * v.shrink * Viewer.defaultMainHeight));
		ammo.setFill(Color.WHITE);

		panel.getChildren().addAll(map, Weapon, ammo);

		double radiusBullet = .5 * v.shrink * HardCodedParameters.bulletHeight;

		for (Bullet b : v.data.getBullets()) {
			Circle bullet = new Circle(radiusBullet, Color.rgb(192, 192, 192));
			bullet.setEffect(new Lighting());
			bullet.setTranslateX(v.shrink * b.getPosition().x + v.shrink * v.xModifier);
			bullet.setTranslateY(v.shrink * b.getPosition().y + v.shrink * v.yModifier);
			panel.getChildren().add(bullet);
		}

		double deadBodySize = .5
				* Math.min(v.shrink * HardCodedParameters.phantomWidth, v.shrink * HardCodedParameters.phantomHeight);

		for (Deadbody deadBody : v.data.getDeadBody()) {
			Circle hitmarker = new Circle(deadBodySize, new ImagePattern(v.deadBodyImage));
			hitmarker.setEffect(new Lighting());
			hitmarker.setTranslateX(v.shrink * deadBody.getP().x + v.shrink * v.xModifier);
			hitmarker.setTranslateY(v.shrink * deadBody.getP().y + v.shrink * v.yModifier);
			panel.getChildren().add(hitmarker);
		}

		for (Enemy e : v.data.getEnemys()) {
			double radiusPhantom = .5 * Math.min(v.shrink * e.getWidth(), v.shrink * e.getHeight());
			
			ImagePattern img = new ImagePattern(v.zombieImage);

			switch (e.getName()) {
			case BLUE:
				img = new ImagePattern(v.zombieImageBlue);
				break;
			case BIG:
				img = new ImagePattern(v.zombieImageBig);
				break;
			default:
				break;

			}

			Circle phantomAvatar = new Circle(radiusPhantom, img);

			phantomAvatar.setEffect(new Lighting());

			phantomAvatar.setTranslateX(v.shrink * e.getPosition().x + v.shrink * v.xModifier);
			phantomAvatar.setTranslateY(v.shrink * e.getPosition().y + v.shrink * v.yModifier);

			Rotate rotation = new Rotate(e.getLookAt(), 0, 0);
			phantomAvatar.getTransforms().add(rotation);
			panel.getChildren().add(phantomAvatar);
			
			panel.getChildren().add(v.getProgressBar(e.getHealth(), e.getInitHeath(), e.getPosition(), e.getWidth(), e.getHeight()));
		}
		
		for (Boss e: v.data.getBoss()) {
			double radiusPhantom = .5 * Math.min(v.shrink * e.getWidth(), v.shrink * e.getHeight());
			
			ImagePattern img = new ImagePattern(v.boss);

			if (e.getHealth() < 50) {
				img = new ImagePattern(v.bossBlood);
			}
			
			Circle phantomAvatar = new Circle(radiusPhantom, img);

			phantomAvatar.setEffect(new Lighting());

			phantomAvatar.setTranslateX(v.shrink * e.getPosition().x + v.shrink * v.xModifier);
			phantomAvatar.setTranslateY(v.shrink * e.getPosition().y + v.shrink * v.yModifier);

			Rotate rotation = new Rotate(e.getLookAt(), 0, 0);
			phantomAvatar.getTransforms().add(rotation);
			panel.getChildren().add(phantomAvatar);
			
			double radiusFlame = v.shrink * HardCodedParameters.flameHeight;
			panel.getChildren().add(v.getProgressBar(e.getHealth(), e.getInitHeath(), e.getPosition(), e.getWidth(), e.getHeight()));

			// deplacement des flammes
			for (Flame f: e.getFlame()) {
				Circle flame = new Circle(radiusFlame, new ImagePattern(v.flameImage));
				flame.setEffect(new Lighting());
				flame.setTranslateX(v.shrink * f.getPosition().x + v.shrink * v.xModifier);
				flame.setTranslateY(v.shrink * f.getPosition().y + v.shrink * v.yModifier);
				
				Rotate rotate = new Rotate(f.getAngle(), 0, 0);
				flame.getTransforms().add(rotate);
				
				panel.getChildren().add(flame);
			}
		}

		Rectangle barreItems = new Rectangle(v.shrink * Viewer.defaultMainWidth * 1.2, v.shrink * Viewer.defaultMainHeight);
		barreItems.setFill(Color.TRANSPARENT);
		barreItems.setStroke(Color.BLACK);
		barreItems.setTranslateX(0);
		barreItems.setTranslateY(v.shrink * Viewer.defaultMainHeight / 1.16);
		
		Rectangle iconeHealthbl = new Rectangle(v.shrink * 50, v.shrink * 50);
		iconeHealthbl.setFill(new ImagePattern(v.iconeHealth));
		iconeHealthbl.setTranslateX(v.shrink * 20);
		iconeHealthbl.setTranslateY(v.shrink * Viewer.defaultMainHeight / 1.12);
		
		ProgressBar healthPg = new ProgressBar((double) v.data.getHero().getHealth() / (double) 100);
		healthPg.setTranslateX(v.shrink * 80);
		healthPg.setTranslateY(v.shrink * Viewer.defaultMainHeight / 1.09);
		
		Rectangle iconeAmmobl = new Rectangle(v.shrink * 50, v.shrink * 50);
		iconeAmmobl.setFill(new ImagePattern(v.iconeAmmo));
		iconeAmmobl.setTranslateX(v.shrink * Viewer.defaultMainWidth * 0.95);
		iconeAmmobl.setTranslateY(v.shrink * Viewer.defaultMainHeight / 1.12);
		
		Rectangle iconeShieldbl = new Rectangle(v.shrink * 50, v.shrink * 50);
		iconeShieldbl.setFill(new ImagePattern(v.iconeShield));
		iconeShieldbl.setTranslateX(v.shrink * Viewer.defaultMainWidth / 5);
		iconeShieldbl.setTranslateY(v.shrink * Viewer.defaultMainHeight / 1.12);

		ProgressBar ArmorPg = new ProgressBar((double) v.data.getHero().getArmor() / (double) 30);
		ArmorPg.setTranslateX(v.shrink * Viewer.defaultMainWidth / 4);
		ArmorPg.setTranslateY(v.shrink * Viewer.defaultMainHeight / 1.09);
		
		Rectangle iconeLevelbl = new Rectangle(v.shrink * 80, v.shrink * 80);
		iconeLevelbl.setFill(new ImagePattern(v.iconeLevelbar));
		iconeLevelbl.setTranslateX(v.shrink * Viewer.defaultMainWidth / 2.7);
		iconeLevelbl.setTranslateY(v.shrink * Viewer.defaultMainHeight / 1.14);
		
		Text greets = new Text(v.shrink * Viewer.defaultMainWidth / 2.55,v.shrink * Viewer.defaultMainHeight / 1.055,"" + v.data.getGame().getCurrentLevel());
		greets.setFill(Color.WHITE);
		greets.setFont(new Font(.05 * v.shrink * Viewer.defaultMainHeight));
		
		Rectangle iconeScorebl = new Rectangle(v.shrink * 55, v.shrink * 55);
		iconeScorebl.setFill(new ImagePattern(v.iconeTrophy));
		iconeScorebl.setTranslateX(v.shrink * Viewer.defaultMainWidth / 2.1);
		iconeScorebl.setTranslateY(v.shrink * Viewer.defaultMainHeight / 1.115);

		Text score = new Text(v.shrink * Viewer.defaultMainWidth / 1.92,v.shrink * Viewer.defaultMainHeight / 1.055, "" + v.data.getScore());
		score.setFill(Color.WHITE);
		score.setFont(new Font(.05 * v.shrink * Viewer.defaultMainHeight));
		
		Rectangle iconeSpeedbl = new Rectangle(v.shrink * 55, v.shrink * 55);
		iconeSpeedbl.setFill(new ImagePattern(v.iconeRun));
		iconeSpeedbl.setTranslateX(v.shrink * Viewer.defaultMainWidth / 1.7);
		iconeSpeedbl.setTranslateY(v.shrink * Viewer.defaultMainHeight / 1.115);
		
		Text speedStat = new Text(v.shrink * Viewer.defaultMainWidth / 1.55,v.shrink * Viewer.defaultMainHeight / 1.055, "" + v.data.getHero().getSpeed());
		speedStat.setFill(Color.WHITE);
		speedStat.setFont(new Font(.05 * v.shrink * Viewer.defaultMainHeight));
		
		ProgressBar shootPg = new ProgressBar((double)1 - ((double) v.data.getHero().timerShooting / (double)v.data.getHero().getCurrentWeapon().getReloadingTime()));
		shootPg.setTranslateX(v.shrink * Viewer.defaultMainWidth *0.8);
		shootPg.setTranslateY(v.shrink * Viewer.defaultMainHeight / 1.10);

		panel.getChildren().addAll(barreItems, speedStat, healthPg, ArmorPg, shootPg,greets, score,iconeHealthbl, iconeAmmobl, iconeShieldbl, iconeLevelbl,iconeScorebl, iconeSpeedbl);

		double hitMarkerSize = .5
				* Math.min(v.shrink * HardCodedParameters.hitMarkerSize, v.shrink * HardCodedParameters.hitMarkerSize);

		for (Hitmarker h : v.data.getHitmarkers()) {
			Circle hitmarker = new Circle(hitMarkerSize, new ImagePattern(v.hitmarkerImage));
			hitmarker.setEffect(new Lighting());
			hitmarker.setTranslateX(v.shrink * h.getP().x + v.shrink * v.xModifier);
			hitmarker.setTranslateY(v.shrink * h.getP().y + v.shrink * v.yModifier);
			panel.getChildren().add(hitmarker);
		}
		for (Loot l : v.data.getLoot()) {
			Circle loot = new Circle();
			switch (l.getBonus().getName()) {
			case AK47:
				loot = new Circle(hitMarkerSize, new ImagePattern(v.ak47));
				break;
			case SHOTGUN:
				loot = new Circle(hitMarkerSize, new ImagePattern(v.shotgun));
				break;
			case LASER:
				loot = new Circle(hitMarkerSize, new ImagePattern(v.laser));
				break;
			case HEALTH:
				loot = new Circle(hitMarkerSize, new ImagePattern(v.heart));
				break;
			case ARMOR:
				loot = new Circle(hitMarkerSize, new ImagePattern(v.shield));
				break;
			case SPEED:
				loot = new Circle(hitMarkerSize, new ImagePattern(v.speed));
				break;
			case DAMAGE:
				loot = new Circle(hitMarkerSize, new ImagePattern(v.chocolat));
				break;
			}

			loot.setTranslateX(v.shrink * l.getPosition().x + v.shrink * v.xModifier);
			loot.setTranslateY(v.shrink * l.getPosition().y + v.shrink * v.yModifier);
			panel.getChildren().add(loot);
		}

		for (Laser l : v.data.getLasers()) {
			Line line = new Line();
			line.setStartX(v.shrink * l.getPosition().x);
			line.setStartY(v.shrink * l.getPosition().y);
			line.setEndX(v.shrink * l.getPositionFinal().x);
			line.setEndY(v.shrink * l.getPositionFinal().y);
			line.setStroke(Color.YELLOW);

			panel.getChildren().add(line);
		}
		
		
		Circle heroesAvatar = new Circle();
		
		switch (v.data.getHero().currentWeapon) {
		case GLOCK:
			Weapon.setFill(new ImagePattern(v.glock));
			if (v.data.getHero().getHealth() <= 50 && v.data.getHero().timerShooting > 0) {
				heroesAvatar = new Circle(radiusHero, new ImagePattern(v.heroGLOCKbloodshooting));
			}

			if (v.data.getHero().getHealth() <= 50 && v.data.getHero().timerShooting <= 0) {
				heroesAvatar = new Circle(radiusHero, new ImagePattern(v.heroGLOCKblood));
			}

			if (v.data.getHero().getHealth() > 50 && v.data.getHero().timerShooting <= 0) {
				heroesAvatar = new Circle(radiusHero, new ImagePattern(v.heroGLOCK));
			}

			if (v.data.getHero().getHealth() > 50 && v.data.getHero().timerShooting > 0) {
				heroesAvatar = new Circle(radiusHero, new ImagePattern(v.heroGLOCKshooting));
			}
			break;
		case AK47:
			Weapon.setFill(new ImagePattern(v.ak47));
			if (v.data.getHero().getHealth() <= 50 && v.data.getHero().timerShooting > 0) {
				heroesAvatar = new Circle(radiusHero, new ImagePattern(v.heroAK47bloodshooting));
			}

			if (v.data.getHero().getHealth() <= 50 && v.data.getHero().timerShooting <= 0) {
				heroesAvatar = new Circle(radiusHero, new ImagePattern(v.heroAK47blood));
			}

			if (v.data.getHero().getHealth() > 50 && v.data.getHero().timerShooting <= 0) {
				heroesAvatar = new Circle(radiusHero, new ImagePattern(v.heroAK47));
			}

			if (v.data.getHero().getHealth() > 50 && v.data.getHero().timerShooting > 0) {
				heroesAvatar = new Circle(radiusHero, new ImagePattern(v.heroAK47shooting));
			}
			break;

		case LASER:
			Weapon.setFill(new ImagePattern(v.laser));
			if (v.data.getHero().getHealth() <= 50 && v.data.getHero().timerShooting > 0) {
				heroesAvatar = new Circle(radiusHero, new ImagePattern(v.heroLASERbloodshooting));
			}

			if (v.data.getHero().getHealth() <= 50 && v.data.getHero().timerShooting <= 0) {
				heroesAvatar = new Circle(radiusHero, new ImagePattern(v.heroLASERblood));
			}

			if (v.data.getHero().getHealth() > 50 && v.data.getHero().timerShooting <= 0) {
				heroesAvatar = new Circle(radiusHero, new ImagePattern(v.heroLASER));
			}

			if (v.data.getHero().getHealth() > 50 && v.data.getHero().timerShooting > 0) {
				heroesAvatar = new Circle(radiusHero, new ImagePattern(v.heroLASERshooting));
			}
			break;
		default:
			Weapon.setFill(new ImagePattern(v.shotgun));
			if (v.data.getHero().getHealth() <= 50 && v.data.getHero().timerShooting > 0) {
				heroesAvatar = new Circle(radiusHero, new ImagePattern(v.heroSHOTGUNbloodshooting));
			}

			if (v.data.getHero().getHealth() <= 50 && v.data.getHero().timerShooting <= 0) {
				heroesAvatar = new Circle(radiusHero, new ImagePattern(v.heroSHOTGUNblood));
			}

			if (v.data.getHero().getHealth() > 50 && v.data.getHero().timerShooting <= 0) {
				heroesAvatar = new Circle(radiusHero, new ImagePattern(v.heroSHOTGUN));
			}

			if (v.data.getHero().getHealth() > 50 && v.data.getHero().timerShooting > 0) {
				heroesAvatar = new Circle(radiusHero, new ImagePattern(v.heroSHOTGUNshooting));
			}
			break;
		}

		heroesAvatar.setEffect(new Lighting());
		heroesAvatar.setTranslateX(v.shrink * v.data.getHero().getPosition().x + v.shrink * v.xModifier);
		heroesAvatar.setTranslateY(v.shrink * v.data.getHero().getPosition().y + v.shrink * v.yModifier);
		Rotate rotationTransform = new Rotate(v.data.getHero().getLookAt(), 0, 0);
		heroesAvatar.getTransforms().add(rotationTransform);
		
		if (v.data.getHero().getLifetimeBonus() > 0) {
			Circle haloCircle = new Circle(radiusHero, new ImagePattern(v.haloBonus));
			haloCircle.setTranslateX(v.data.getHero().getPosition().x * v.shrink + v.shrink * v.xModifier);
			haloCircle.setTranslateY(v.data.getHero().getPosition().y * v.shrink + v.shrink * v.yModifier);
			panel.getChildren().add(haloCircle);
		}

		panel.getChildren().add(heroesAvatar);

		if (v.data.getIsDebug()) {
			ListView<String> list = new ListView<String>();
			ObservableList<String> items =FXCollections.observableArrayList ();
			items.add("Console de Debug :");
			for(String l : v.data.getDebug().getLogs()) {
				items.add(l);
			}
			list.setItems(items);
			panel.getChildren().add(list);
		}

		return panel;
	}

}
