/* ******************************************************
 * Project alpha - Composants logiciels 2015.
 * Copyright (C) 2015 <Binh-Minh.Bui-Xuan@ens-lyon.org>.
 * GPL version>=3 <http://www.gnu.org/licenses/>.
 * $Id: userInterface/Viewer.java 2015-03-11 buixuan.
 * ******************************************************/
package userInterface;

import tools.HardCodedParameters;
import tools.Position;
import tools.User;
import specifications.ViewerService;
import specifications.ReadService;
import specifications.RequireEngineViewService;
import specifications.RequireReadService;
import specifications.ChildViewService;
import specifications.EngineViewerService;

import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.effect.Lighting;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.scene.transform.Rotate;
import javafx.scene.text.Font;
import javafx.scene.image.Image;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


import data.Boss;
import data.Bullet;
import data.Deadbody;
import data.Enemy;
import data.Flame;
import data.Hitmarker;
import data.Laser;
import data.Map;
import data.Loot;
import data.Weapon;

public class Viewer implements ViewerService, RequireReadService, RequireEngineViewService {
	public static final double defaultMainWidth = HardCodedParameters.defaultWidth,
			defaultMainHeight = HardCodedParameters.defaultHeight, weaponHeight = HardCodedParameters.weapontHeight,
			weaponWidth = HardCodedParameters.weaponWidth;
	public ReadService data;
	
	private ChildViewService scoresView, menuView, gameView, settingsView;

	public Image backgroundMainMenu;

	// background des maps
	public Image backgroundMapHell;
	public Image backgroundMapHeaven;

	public Image ImageTitle;
	public Image btJouer;
	public Image btCommencer;
	public Image btQuitter;
	public Image btRejouer;

	public Image btFacile;
	public Image btNormal;
	public Image btDifficile;
	public Image btVeteran;
	
	public Image iconeHealth;
	public Image iconeAmmo;
	public Image iconeShield;
	public Image iconeLevelbar;
	public Image iconeTrophy;
	public Image iconeRun;

	public Image flameImage;
	public Image zombieImage;
	public Image zombieImageBlue;
	public Image zombieImageBig;
	public Image zombieLeftImage;
	public Image hitmarkerImage;
	public Image deadBodyImage;

	public Image bossBlood;
	public Image boss;

	// AK47
	public Image ak47;
	public Image heroAK47;
	public Image heroAK47blood;
	public Image heroAK47shooting;
	public Image heroAK47bloodshooting;

	// LASER
	public Image laser;
	public Image heroLASER;
	public Image heroLASERblood;
	public Image heroLASERshooting;
	public Image heroLASERbloodshooting;

	// SHOTGUN
	public Image shotgun;
	public Image heroSHOTGUN;
	public Image heroSHOTGUNblood;
	public Image heroSHOTGUNbloodshooting;
	public Image heroSHOTGUNshooting;

	// GLOCK
	public Image glock;
	public Image heroGLOCK;
	public Image heroGLOCKblood;
	public Image heroGLOCKbloodshooting;
	public Image heroGLOCKshooting;
	
	// halo
	public Image haloBonus;

	public double xShrink, yShrink, shrink, xModifier, yModifier;

	public Image heart, shield, speed, chocolat;
	public Position mousePos;
	public EngineViewerService engine;

	public Viewer() {
	}

	@Override
	public void init() {
		xShrink = 1;
		yShrink = 1;
		xModifier = 0;
		yModifier = 0;
		haloBonus = new Image("file:src/images/halo.png");
		
		backgroundMainMenu = new Image("file:src/images/bkg0.jpg");

		backgroundMapHeaven = new Image("file:src/images/bkg2.png");
		backgroundMapHell = new Image("file:src/images/street-background.png");

		ImageTitle = new Image("file:src/images/titre.png");

		btJouer = new Image("file:src/images/btJouer.png");
		btCommencer = new Image("file:src/images/btCommencer.png");
		btQuitter = new Image("file:src/images/btQuitter.png");
		btRejouer = new Image("file:src/images/btRejouer.png");

		btFacile = new Image("file:src/images/btFacile.png");
		btNormal = new Image("file:src/images/btNormal.png");
		btDifficile = new Image("file:src/images/btDifficile.png");
		btVeteran = new Image("file:src/images/btVeteran.png");

		
		iconeHealth = new Image("file:src/images/croix.png");
		iconeAmmo = new Image("file:src/images/bullet.png");
		iconeShield = new Image("file:src/images/shieldbar.png");
		iconeLevelbar = new Image("file:src/images/level.png");
		iconeTrophy = new Image("file:src/images/trophy.png");
		iconeRun  = new Image("file:src/images/run.png");

		zombieImage = new Image("file:src/images/zombie.png");
		zombieLeftImage = new Image("file:src/images/zombiestoLeft.png");
		zombieImageBlue = new Image("file:src/images/zombie-blue.png");
		zombieImageBig = new Image("file:src/images/zombie-big.png");

		bossBlood = new Image("file:src/images/finalboss_blood.png");
		boss = new Image("file:src/images/finalboss.png");
		flameImage = new Image("file:src/images/flame.png");
		
		heart = new Image("file:src/images/health.png");
		shield = new Image("file:src/images/shield.png");
		speed = new Image("file:src/images/flying_shooes.png");
		chocolat = new Image("file:src/images/chocolat.png");

		hitmarkerImage = new Image("file:src/images/hitmarker.png");
		deadBodyImage = new Image("file:src/images/zombies_dead.png");

		heroGLOCK = new Image("file:src/images/heroGLOCK.png");
		heroGLOCKblood = new Image("file:src/images/heroGLOCKblood.png");
		heroGLOCKbloodshooting = new Image("file:src/images/heroGLOCKbloodshooting.png");
		heroGLOCKshooting = new Image("file:src/images/heroGLOCKshooting.png");

		heroAK47 = new Image("file:src/images/heroAK47.png");
		heroAK47blood = new Image("file:src/images/heroAK47blood.png");
		heroAK47bloodshooting = new Image("file:src/images/heroAK47bloodshooting.png");
		heroAK47shooting = new Image("file:src/images/heroAK47shooting.png");

		heroLASER = new Image("file:src/images/heroLASER.png");
		heroLASERblood = new Image("file:src/images/heroLASERblood.png");
		heroLASERbloodshooting = new Image("file:src/images/heroLASERbloodshoot.png");
		heroLASERshooting = new Image("file:src/images/heroLASERshoot.png");

		heroSHOTGUN = new Image("file:src/images/heroSHOTGUN.png");
		heroSHOTGUNblood = new Image("file:src/images/heroSHOTGUNblood.png");
		heroSHOTGUNbloodshooting = new Image("file:src/images/heroSHOTGUNbloodshooting.png");
		heroSHOTGUNshooting = new Image("file:src/images/heroSHOTGUNshooting.png");

		ak47 = new Image("file:src/images/AK47.png");
		shotgun = new Image("file:src/images/SHOTGUN.png");
		glock = new Image("file:src/images/GLOCK.png");
		laser = new Image("file:src/images/laser.png");
		
		scoresView = new Scores();
		menuView =  new Menu();
		gameView = new Game();
		settingsView = new Settings();
		scoresView.onBindParentView(this);
		menuView.onBindParentView(this);
		gameView.onBindParentView(this);
		settingsView.onBindParentView(this);
		
	}

	@Override
	public void bindReadService(ReadService service) {
		data = service;
	}

	public void updateShrink() {
		shrink = Math.min(xShrink, yShrink);
		xModifier = .01 * shrink * defaultMainWidth;
		yModifier = .01 * shrink * defaultMainHeight;
	}

	@Override
	public Parent getPanel() {
		updateShrink();
		Group panel = new Group();

		try {
			switch (engine.getGameInterface()) {
			case MAIN_MENU:
				panel = menuView.getPanel();
				break;
			case GAME_SETTINGS:
				panel = settingsView.getPanel();
				break;
			case GAME:
				panel = gameView.getPanel();
				break;
			default:
				// default: score
				panel = scoresView.getPanel();
				break;
			}
			
			panel.getStylesheets().add("file:src/css/design.css");
		} catch (java.util.ConcurrentModificationException e) {
			return panel;
		}

		return panel;
	}



	public ProgressBar getProgressBar(double currentValue, double totalValue, Position p, double width, double height) {
		double pourcentage = (double) currentValue / (double) totalValue;
		ProgressBar progressbar = new ProgressBar(pourcentage);
		progressbar.setTranslateX(shrink * p.x - shrink * width / (double)2);
		progressbar.setTranslateY(shrink * p.y - shrink * height / (double)2);
		
		if (pourcentage < 0.20) {
			progressbar.getStyleClass().add("red-bar");
		} else if (pourcentage < 0.50) {
			progressbar.getStyleClass().add("orange-bar");
		} else if (pourcentage < 0.75) {
			progressbar.getStyleClass().add("yellow-bar");
		} else {
			progressbar.getStyleClass().add("green-bar");
		}
		
		progressbar.setMaxWidth(shrink * width);

		progressbar.setOpacity(0.3);
		return progressbar;
	}

	
	@Override
	public void setMainWindowWidth(double width) {
		xShrink = width / defaultMainWidth;
	}

	@Override
	public void setMainWindowHeight(double height) {
		yShrink = height / defaultMainHeight;
	}

	@Override
	public void setMousePosition(Position position) {
		mousePos = position;
		Position absoluteMousePos = new Position((-xModifier + mousePos.x) / (double) shrink,
				(-yModifier + mousePos.y) / (double) shrink

		);
		engine.setMousePosition(absoluteMousePos);
	}

	@Override
	public void userClickMouse() {
		Position absoluteMousePos = new Position((-xModifier + mousePos.x) / (double) shrink,
				(-yModifier + mousePos.y) / (double) shrink);
		engine.setMousePosition(absoluteMousePos);
		if (engine.getGameInterface() == User.INTERFACE.GAME) {
			engine.heroShoot();
		}
	}

	@Override
	public void bindEngineViewService(EngineViewerService service) {
		engine = service;
	}

}
