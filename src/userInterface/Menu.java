package userInterface;

import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import specifications.ChildViewService;

public class Menu implements ChildViewService {
	private Viewer v;

	@Override
	public void onBindParentView(Viewer v) {
		this.v = v;
	}

	@Override
	public Group getPanel() {
		Group panel = new Group();

		Rectangle map = new Rectangle(v.shrink * Viewer.defaultMainWidth * 1.3, v.shrink *  Viewer.defaultMainHeight);
		map.setFill(new ImagePattern( v.backgroundMainMenu));
		map.setStroke(Color.WHITE);
		map.setTranslateX(0);
		map.setTranslateY(0);
		panel.getChildren().add(map);

		Rectangle BlockTitleImg = new Rectangle( v.shrink *  Viewer.defaultMainWidth / 2.1,  v.shrink *  Viewer.defaultMainHeight / 2);
		BlockTitleImg.setFill(new ImagePattern( v.ImageTitle));
		BlockTitleImg.setTranslateX( v.shrink *  Viewer.defaultMainWidth / 3.7);
		BlockTitleImg.setTranslateY( v.shrink *  Viewer.defaultMainHeight / 5.5);
		panel.getChildren().add(BlockTitleImg);

		Rectangle BlockImageNathan = new Rectangle( v.shrink *  Viewer.defaultMainWidth / 8,  v.shrink *  Viewer.defaultMainHeight / 5);
		BlockImageNathan.setFill(new ImagePattern( v.heroGLOCK));
		BlockImageNathan.setTranslateX( v.shrink *  Viewer.defaultMainWidth / 7);
		BlockImageNathan.setTranslateY( v.shrink *  Viewer.defaultMainHeight / 3);
		panel.getChildren().add(BlockImageNathan);

		Rectangle BlockImageZombies = new Rectangle( v.shrink *  Viewer.defaultMainWidth / 8,  v.shrink *  Viewer.defaultMainHeight / 5);
		BlockImageZombies.setFill(new ImagePattern( v.zombieLeftImage));
		BlockImageZombies.setTranslateX( v.shrink *  Viewer.defaultMainWidth / 1.32);
		BlockImageZombies.setTranslateY( v.shrink *  Viewer.defaultMainHeight / 2.8);
		panel.getChildren().add(BlockImageZombies);

		Rectangle btnPlay = new Rectangle( v.shrink * 200,  v.shrink * 60);
		btnPlay.setOnMousePressed(evt ->  v.engine.userClickPlaySettings());
		btnPlay.setFill(new ImagePattern( v.btJouer));
		btnPlay.setTranslateX( v.shrink *  Viewer.defaultMainWidth / 3.2);
		btnPlay.setTranslateY( v.shrink *  Viewer.defaultMainHeight / 1.4);
		panel.getChildren().add(btnPlay);

		Rectangle btnQuit = new Rectangle( v.shrink * 200,  v.shrink * 60);
		btnQuit.setOnMousePressed(evt ->  v.engine.userClickQuit());
		btnQuit.setFill(new ImagePattern( v.btQuitter));
		btnQuit.setTranslateX( v.shrink *  Viewer.defaultMainWidth / 1.8);
		btnQuit.setTranslateY( v.shrink *  Viewer.defaultMainHeight / 1.4);
		panel.getChildren().add(btnQuit);

		return panel;
	}

}
