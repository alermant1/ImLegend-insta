package userInterface;

import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import specifications.ChildViewService;

public class Scores implements ChildViewService {
	private Viewer v;

	@Override
	public void onBindParentView(Viewer v) {
		this.v = v;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Group getPanel() {
		Group panel = new Group();

		Rectangle map = new Rectangle(v.shrink * Viewer.defaultMainWidth * 1.3, v.shrink * Viewer.defaultMainHeight);
		map.setFill(new ImagePattern(v.backgroundMainMenu));
		map.setStroke(Color.WHITE);
		map.setTranslateX(0);
		map.setTranslateY(0);
		panel.getChildren().add(map);

		Text gameOver = new Text(v.shrink * Viewer.defaultMainWidth / 3, v.shrink * Viewer.defaultMainHeight / 8,
				"Fin de la partie");
		gameOver.setFont(new Font(.1 * v.shrink * Viewer.defaultMainHeight));
		panel.getChildren().add(gameOver);

		Text score = new Text(v.shrink * Viewer.defaultMainWidth / 1.3, v.shrink * Viewer.defaultMainHeight / 2.2,
				"Score: " + v.data.getScore());
		score.setFont(new Font(.05 * v.shrink * Viewer.defaultMainHeight));
		panel.getChildren().add(score);

		Text TotalBullet = new Text(v.shrink * Viewer.defaultMainWidth / 1.3, v.shrink * Viewer.defaultMainHeight / 1.9,
				"Balles Tirees: " + v.data.getStats().getTotalBulletsShots());
		TotalBullet.setFont(new Font(.05 * v.shrink * Viewer.defaultMainHeight));
		panel.getChildren().add(TotalBullet);

		Text TotalDommage = new Text(v.shrink * Viewer.defaultMainWidth / 1.3, v.shrink * Viewer.defaultMainHeight / 1.68,
				"Dommage pris: " + v.data.getStats().getTotalDamageTaken());
		TotalDommage.setFont(new Font(.05 * v.shrink * Viewer.defaultMainHeight));
		panel.getChildren().add(TotalDommage);

		Text Totalkill = new Text(v.shrink * Viewer.defaultMainWidth / 1.3, v.shrink * Viewer.defaultMainHeight / 1.5,
				"Enemies tues: " + v.data.getStats().getTotalZombieKilled());
		Totalkill.setFont(new Font(.05 * v.shrink * Viewer.defaultMainHeight));
		panel.getChildren().add(Totalkill);

		final CategoryAxis xAxis = new CategoryAxis();
		final NumberAxis yAxis = new NumberAxis();
		final LineChart<String, Number> lineChart = new LineChart<String, Number>(xAxis, yAxis);

		XYChart.Series<String, Number> zombiesKilled = new XYChart.Series<String, Number>();
		zombiesKilled.setName("Zombie killed");
		for (java.util.Map.Entry<Integer, Integer> entry : v.data.getStats().getZombieKilled().entrySet()) {
			zombiesKilled.getData().add(new XYChart.Data<String, Number>(entry.getKey().toString(), entry.getValue()));
		}

		XYChart.Series<String, Number> bulletShots = new XYChart.Series<String, Number>();
		bulletShots.setName("Bullets shots");
		for (java.util.Map.Entry<Integer, Integer> entry : v.data.getStats().getBulletShots().entrySet()) {
			bulletShots.getData().add(new XYChart.Data<String, Number>(entry.getKey().toString(), entry.getValue()));
		}

		XYChart.Series<String, Number> damageTaken = new XYChart.Series<String, Number>();
		damageTaken.setName("Damage taken");
		for (java.util.Map.Entry<Integer, Integer> entry : v.data.getStats().getDamageTaken().entrySet()) {
			damageTaken.getData().add(new XYChart.Data<String, Number>(entry.getKey().toString(), entry.getValue()));
		}

		lineChart.getData().addAll(damageTaken, bulletShots, zombiesKilled);
		lineChart.setTranslateX(v.shrink * Viewer.defaultMainWidth / 6);
		lineChart.setTranslateY(v.shrink * Viewer.defaultMainHeight / 5);
		lineChart.setMaxHeight(v.shrink * 400);
		lineChart.setMaxWidth(v.shrink * 400);
		panel.getChildren().add(lineChart);

		Rectangle btnPlay = new Rectangle(v.shrink * 200, v.shrink * 60);
		btnPlay.setOnMousePressed(evt -> v.engine.userClickPlaySettings());
		btnPlay.setFill(new ImagePattern(v.btRejouer));
		btnPlay.setTranslateX(v.shrink * Viewer.defaultMainWidth / 3.2);
		btnPlay.setTranslateY(v.shrink * Viewer.defaultMainHeight / 1.2);
		panel.getChildren().add(btnPlay);

		Rectangle btnQuit = new Rectangle(v.shrink * 200, v.shrink * 60);
		btnQuit.setOnMousePressed(evt -> v.engine.userClickQuit());
		btnQuit.setFill(new ImagePattern(v.btQuitter));
		btnQuit.setTranslateX(v.shrink * Viewer.defaultMainWidth / 1.8);
		btnQuit.setTranslateY(v.shrink * Viewer.defaultMainHeight / 1.2);
		panel.getChildren().add(btnQuit);

		return panel;
	}

}
