package userInterface;

import data.Map;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import specifications.ChildViewService;

public class Settings implements ChildViewService {
	private Viewer v;

	@Override
	public void onBindParentView(Viewer v) {
		this.v = v;
	}

	@Override
	public Group getPanel() {

		Group panel = new Group();

		Rectangle map = new Rectangle(v.shrink * Viewer.defaultMainWidth * 1.3, v.shrink * Viewer.defaultMainHeight);
		
		
		
		map.setFill(new ImagePattern(v.backgroundMainMenu));
		map.setTranslateX(0);
		map.setTranslateY(0);
		panel.getChildren().add(map);

		Text SelectMapTitle = new Text(v.shrink * Viewer.defaultMainWidth / 2.5, v.shrink * Viewer.defaultMainHeight / 8,
				"Selectionnez une carte");
		SelectMapTitle.setFont(new Font(.04 * v.shrink * Viewer.defaultMainHeight));
		panel.getChildren().add(SelectMapTitle);

		Text SelectLvlTitle = new Text(v.shrink * Viewer.defaultMainWidth / 2.5, v.shrink * Viewer.defaultMainHeight / 1.7,
				"Selectionnez un niveau");
		SelectLvlTitle.setFont(new Font(.04 * v.shrink * Viewer.defaultMainHeight));
		panel.getChildren().add(SelectLvlTitle);

		int index = 0;

		for (Map availableMap : v.data.getAvailableMap()) {

			Rectangle btnMap = new Rectangle(v.shrink * 200, v.shrink * 190);
			btnMap.setFill(new ImagePattern(new Image(availableMap.getBackgroundMenu())));

			btnMap.setTranslateX(v.shrink * Viewer.defaultMainWidth / 3 + (v.shrink * 200 + 70 * v.shrink) * index);
			btnMap.setTranslateY(v.shrink * Viewer.defaultMainHeight / 4.4);

			if (v.data.getGame().getMap() == null && index == 0) {
				v.data.getGame().setMap(availableMap);
			}

			if (v.data.getGame().getMap().getMapName() == availableMap.getMapName()) {
				btnMap.setStroke(Color.GREEN);
			}

			btnMap.setOnMousePressed(evt -> v.data.getGame().setMap(availableMap));
			panel.getChildren().add(btnMap);

			index++;
		}

		Rectangle btnDiffEasy = new Rectangle(v.shrink * 80, v.shrink * 40);
		btnDiffEasy.setFill(new ImagePattern(v.btFacile));
		btnDiffEasy.setTranslateX(v.shrink * Viewer.defaultMainWidth / 2.6);
		btnDiffEasy.setTranslateY(v.shrink * Viewer.defaultMainHeight / 1.5);
		if (v.data.getGame().getDifficulty() == 1) {
			btnDiffEasy.setStroke(Color.GREEN);
		}
		btnDiffEasy.setOnMousePressed(evt -> v.data.getGame().setDifficulty(1));
		panel.getChildren().add(btnDiffEasy);

		Rectangle btnDiffMedium = new Rectangle(v.shrink * 80, v.shrink * 40);
		btnDiffMedium.setFill(new ImagePattern(v.btNormal));
		btnDiffMedium.setTranslateX(v.shrink * Viewer.defaultMainWidth / 2.2);
		btnDiffMedium.setTranslateY(v.shrink * Viewer.defaultMainHeight / 1.5);
		if (v.data.getGame().getDifficulty() == 2) {
			btnDiffMedium.setStroke(Color.GREEN);
		}
		btnDiffMedium.setOnMousePressed(evt -> v.data.getGame().setDifficulty(2));
		panel.getChildren().add(btnDiffMedium);

		Rectangle btnDiffHard = new Rectangle(v.shrink * 80, v.shrink * 40);
		btnDiffHard.setFill(new ImagePattern(v.btDifficile));
		btnDiffHard.setTranslateX(v.shrink * Viewer.defaultMainWidth / 1.9);
		btnDiffHard.setTranslateY(v.shrink * Viewer.defaultMainHeight / 1.5);
		if (v.data.getGame().getDifficulty() == 4) {
			btnDiffHard.setStroke(Color.GREEN);
		}
		btnDiffHard.setOnMousePressed(evt -> v.data.getGame().setDifficulty(4));
		panel.getChildren().add(btnDiffHard);

		Rectangle btnDiffVeteran = new Rectangle(v.shrink * 80, v.shrink * 40);
		btnDiffVeteran.setFill(new ImagePattern(v.btVeteran));
		btnDiffVeteran.setTranslateX(v.shrink * Viewer.defaultMainWidth / 1.68);
		btnDiffVeteran.setTranslateY(v.shrink * Viewer.defaultMainHeight / 1.5);
		if (v.data.getGame().getDifficulty() == 10) {
			btnDiffVeteran.setStroke(Color.GREEN);
		}
		btnDiffVeteran.setOnMousePressed(evt -> v.data.getGame().setDifficulty(10));
		panel.getChildren().add(btnDiffVeteran);

		Rectangle btnPlay2 = new Rectangle(v.shrink * 200, v.shrink * 60);
		btnPlay2.setOnMousePressed(evt -> v.engine.userClickPlay());
		btnPlay2.setFill(new ImagePattern(v.btCommencer));
		btnPlay2.setTranslateX(v.shrink * Viewer.defaultMainWidth / 1.2);
		btnPlay2.setTranslateY(v.shrink * Viewer.defaultMainHeight / 1.14);
		panel.getChildren().add(btnPlay2);

		return panel;
	}

}
