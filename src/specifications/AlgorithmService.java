/* ******************************************************
 * Project alpha - Composants logiciels 2015.
 * Copyright (C) 2015 <Binh-Minh.Bui-Xuan@ens-lyon.org>.
 * GPL version>=3 <http://www.gnu.org/licenses/>.
 * $Id: specifications/AlgorithmService.java 2015-03-11 buixuan.
 * ******************************************************/
package specifications;

import tools.Position;

public interface AlgorithmService {
	public void init();
	public Position generateSpawnBorder();
	public void bindEngineService(EngineService s);
	public void bindDataService(DataService s);
	public void randomLoot(Position e) ;
	public void spawnEnemy();
	public void generateEnemy();
	public void generateBoss();
}
