package specifications;

public interface RequireEngineViewService {
	public void bindEngineViewService(EngineViewerService service);
}
