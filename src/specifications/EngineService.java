/* ******************************************************
 * Project alpha - Composants logiciels 2015.
 * Copyright (C) 2015 <Binh-Minh.Bui-Xuan@ens-lyon.org>.
 * GPL version>=3 <http://www.gnu.org/licenses/>.
 * $Id: specifications/EngineService.java 2015-03-11 buixuan.
 * ******************************************************/
package specifications;

import tools.Position;
import tools.User;
import tools.User.WEAPON;

public interface EngineService {
	public void init();

	public void start();

	public void stop();

	public void togglePause();

	public void setHeroesCommand(User.COMMAND c);

	public void releaseHeroesCommand(User.COMMAND c);

	public void setMousePosition(Position pos);

	public void heroShoot();

	public void toggleDebug();

	public void setWeapon(WEAPON glock);

	public void setGameInterface(User.INTERFACE i);

	public void bindRandomAiService(AlgorithmService randomAi2);

	public int getPreviousLevel();

	public void setPreviousLevel(int previousLevel);

	public User.INTERFACE getGameInterface();
}
