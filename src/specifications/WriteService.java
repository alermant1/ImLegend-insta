/* ******************************************************
 * Project alpha - Composants logiciels 2015.
 * Copyright (C) 2015 <Binh-Minh.Bui-Xuan@ens-lyon.org>.
 * GPL version>=3 <http://www.gnu.org/licenses/>.
 * $Id: specifications/WriteService.java 2015-03-11 buixuan.
 * ******************************************************/
package specifications;

import tools.Position;
import tools.Sound;

import java.util.ArrayList;

import data.Boss;
import data.Bullet;
import data.Deadbody;
import data.Enemy;
import data.Flame;
import data.Hitmarker;
import data.Laser;
import data.Loot;
import data.Map;
import data.StaticObject;

public interface WriteService {
	public void setHeroesPosition(Position p);

	public void setStepNumber(int n);

	public void setSoundEffect(ArrayList<Sound.SOUND> s);

	public void addScore(int score);

	public void addBullet(Bullet bullet);

	public void removeBullet(int index);

	public void setBullet(ArrayList<Bullet> b);

	public void addLaser(Laser l);

	public void removeLaser(int index);

	public void setLaser(ArrayList<Laser> l);

	public void setLoot(ArrayList<Loot> newloot);

	public void addEnemy(Enemy e);

	public void deleteEnemy(int i);

	public void setEnemys(ArrayList<Enemy> enemys);

	public void initGame(Map m, int difficulty, boolean debugMode);

	public void resetSoundEffect();

	public void addHitMarker(Hitmarker m);

	public void removeHitMarker(int i);

	public void setHitMarkers(ArrayList<Hitmarker> hs);

	public void setDeadBody(ArrayList<Deadbody> deadBody);

	public void addBoss(Boss b);

	public void removeBoss(int index);

	public void bossShoot(int index, Flame b);
	
	public void setBoss(ArrayList<Boss> boss);

	public void setDebug(String log);
	
	public void setIsDebug(boolean bool);
}
