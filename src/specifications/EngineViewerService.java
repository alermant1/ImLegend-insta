package specifications;

import tools.Position;
import tools.User;

public interface EngineViewerService {
	public void setMousePosition(Position pos);
	public void heroShoot();
	public User.INTERFACE getGameInterface();
	public void userClickQuit();
	public void userClickPlaySettings();
	public void userClickPlay();
	public void userClickScore();
}
