package specifications;

import tools.Position;

public interface ViewerEngineService {
	public void setMousePosition(Position pos);

	public void heroShoot();
}
