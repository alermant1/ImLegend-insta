package specifications;

import javafx.scene.Group;
import userInterface.Viewer;

public interface ChildViewService {
	
	public void onBindParentView(Viewer v );
	
	public Group getPanel();

}
