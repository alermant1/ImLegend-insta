/* ******************************************************
 * Project alpha - Composants logiciels 2015.
 * Copyright (C) 2015 <Binh-Minh.Bui-Xuan@ens-lyon.org>.
 * GPL version>=3 <http://www.gnu.org/licenses/>.
 * $Id: specifications/ReadService.java 2015-03-11 buixuan.
 * ******************************************************/
package specifications;

import tools.Position;
import tools.Sound;

import java.util.ArrayList;
import java.util.List;

import data.Boss;
import data.Bullet;
import data.Deadbody;
import data.Debug;
import data.Enemy;
import data.Game;
import data.Hero;
import data.Hitmarker;
import data.Laser;
import data.Loot;
import data.Map;
import data.Stats;
import data.Weapon;

public interface ReadService {
	public Position getHeroesPosition();

	public Hero getHero();

	public int getScore();

	public ArrayList<Enemy> getEnemys();

	ArrayList<Loot> getLoot();

	public ArrayList<Sound.SOUND> getSoundEffect();

	public List<Bullet> getBullets();

	public List<Laser> getLasers();

	public Game getGame();

	public ArrayList<Map> getAvailableMap();

	public ArrayList<Hitmarker> getHitmarkers();

	public ArrayList<Deadbody> getDeadBody();

	public Stats getStats();

	public Weapon getAk47();

	public Weapon getLaserWeapon();

	public Weapon getShotgun();

	public ArrayList<Boss> getBoss();

	public Debug getDebug();

	public boolean getIsDebug();
}
