/* ******************************************************
 * Project alpha - Composants logiciels 2015.
 * Copyright (C) 2015 <Binh-Minh.Bui-Xuan@ens-lyon.org>.
 * GPL version>=3 <http://www.gnu.org/licenses/>.
 * $Id: algorithm/RandomWalker.java 2015-03-11 buixuan.
 * ******************************************************/
package algorithm;

import tools.HardCodedParameters;
import tools.Position;
import tools.Sound;
import tools.User;

import specifications.AlgorithmService;
import specifications.DataService;
import specifications.EngineService;
import specifications.RequireEngineService;

import java.util.Random;

import data.Bonus;
import data.Boss;
import data.Enemy;
import data.Loot;
import data.Weapon;

public class GameAlgo implements AlgorithmService, RequireEngineService {
	private EngineService engine;
	private DataService data;
	private Random gen;

	public GameAlgo() {
	}

	@Override
	public void bindEngineService(EngineService service) {
		engine = service;
	}

	@Override
	public void init() {
		gen = new Random();
	}

	@Override
	public Position generateSpawnBorder() {

		switch (gen.nextInt(4)) {
		case 0:
			// haut
			return new Position(gen.nextInt(HardCodedParameters.defaultWidth), 10);
		case 1:
			// gauche
			return new Position(10,
					gen.nextInt(HardCodedParameters.defaultHeight - HardCodedParameters.limitPositionBottom));
		case 2:
			// bas
			return new Position(gen.nextInt(HardCodedParameters.defaultWidth),
					HardCodedParameters.defaultHeight - HardCodedParameters.limitPositionBottom);
		default:
			// droite
			return new Position(HardCodedParameters.defaultWidth,
					gen.nextInt(HardCodedParameters.defaultHeight - HardCodedParameters.limitPositionBottom));

		}
	}

	public void randomLoot(Position e) {
		Loot loot = new Loot();
		Bonus bonus = new Bonus();
		switch (gen.nextInt(7)) {
		case 0:
			Weapon ak47 = new Weapon();
			ak47.setBulletDamage(2);
			ak47.setAmmo(100);
			ak47.reloadingTime = HardCodedParameters.reloadingTimeAK47;
			ak47.setBulletSpeed(0.05);
			ak47.setName(User.WEAPON.AK47);
			bonus.setWeapon(ak47);
			loot.setLifeTime(40);
			loot.setBackground("ak47");
			bonus.setName(tools.Bonus.TYPE.AK47);
			loot.setBonus(bonus);
			loot.setPosition(e);
			break;
		case 1:
			Weapon shotgun = new Weapon();
			shotgun.setBulletDamage(2);
			shotgun.setAmmo(100);
			shotgun.reloadingTime = HardCodedParameters.reloadingTimeSHOTGUN;
			shotgun.setBulletSpeed(0.05);
			shotgun.setName(User.WEAPON.SHOTGUN);
			bonus.setWeapon(shotgun);
			loot.setLifeTime(40);
			bonus.setName(tools.Bonus.TYPE.SHOTGUN);
			loot.setBackground("shotgun");
			loot.setBonus(bonus);
			loot.setPosition(e);
			break;
		case 2:
			bonus.setMoreArmor(10);
			loot.setLifeTime(40);
			bonus.setName(tools.Bonus.TYPE.ARMOR);
			loot.setBackground("Armor");
			loot.setBonus(bonus);
			loot.setPosition(e);
			break;
		case 3:
			bonus.setMoreHealth(20);
			loot.setLifeTime(40);
			bonus.setName(tools.Bonus.TYPE.HEALTH);
			loot.setBackground("Health");
			loot.setBonus(bonus);
			loot.setPosition(e);
			break;
		case 4:
			bonus.setMoreSpeed(10);
			bonus.setLifetime(50);
			bonus.setName(tools.Bonus.TYPE.SPEED);
			loot.setLifeTime(40);
			loot.setBackground("Speed");
			loot.setBonus(bonus);
			loot.setPosition(e);
			break;
		case 5:
			bonus.SetMoreDomage(5);
			bonus.setLifetime(100);
			bonus.setName(tools.Bonus.TYPE.DAMAGE);
			loot.setLifeTime(40);
			loot.setBackground("Damage");
			loot.setBonus(bonus);
			loot.setPosition(e);
			break;
		case 6:
			Weapon laser = new Weapon();
			laser.setBulletDamage(2);
			laser.setAmmo(5);
			laser.reloadingTime = HardCodedParameters.reloadingTimeLASER;
			laser.setBulletSpeed(0.05);
			laser.setName(User.WEAPON.LASER);
			bonus.setWeapon(laser);
			loot.setLifeTime(40);
			bonus.setName(tools.Bonus.TYPE.LASER);
			loot.setBackground("laser");
			loot.setBonus(bonus);
			loot.setPosition(e);
			break;
		}
		data.setDebug("Drop loot");
		data.getLoot().add(loot);
	}

	@Override
	public void bindDataService(DataService s) {
		this.data = s;
	}
	
	public void spawnEnemy() {
		double x = 0;
		double y = 0;
		boolean cont = true;
		while (cont) {
			Position p = this.generateSpawnBorder();
			y = p.y;
			x = p.x;
			cont = false;
			for (Enemy e : data.getEnemys()) {
				if (e.getPosition().equals(new Position(x, y)))
					cont = true;
			}
		}

		// on recupere un enemy aleatoire dans la liste des enemys disponible
		// sur cette map

		Enemy selectedEnemy = data.getGame().getMap().getEnemys()
				.get(gen.nextInt(data.getGame().getMap().getEnemys().size()));
		Enemy newEnemy = selectedEnemy.getClone();

		newEnemy.setPosition(new Position(x, y));

		data.addEnemy(newEnemy);
	}
	
	public void generateBoss() {
		Boss b = new Boss();
		b.setPosition(this.generateSpawnBorder());
		b.setInitHeath(1000);
		b.setHeight(100);
		b.setWidth(100);
		b.setHitDamage(15);
		b.setSpeed(0.003);
		data.addBoss(b);
	}

	public void generateEnemy() {
		// apparation des zombies
		if (data.getEnemys().size() < HardCodedParameters.minZombie + data.getGame().getCurrentLevel()
				+ data.getGame().getDifficulty()) {
			this.spawnEnemy();
		}

		// on fait apparaitre x boss a chaque niveau
		if (engine.getPreviousLevel() != data.getGame().getCurrentLevel() && data.getGame().getScore() % 1000 == 0
				&& data.getGame().getScore() > 0) {
			engine.setPreviousLevel(data.getGame().getCurrentLevel());
			generateBoss();

			if (data.getGame().getCurrentLevel() > 10) {
				generateBoss();
			}
			data.getSoundEffect().add(Sound.SOUND.BOSS);
		}

		// apparition d'un zombie aleatoirement selon le niveau & la difficulte
		if (gen.nextInt(100) < 1 + data.getGame().getDifficulty() + data.getGame().getCurrentLevel() / 10)
			this.spawnEnemy();
	}


}
