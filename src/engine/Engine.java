/* ******************************************************
 * Project alpha - Composants logiciels 2015.
 * Copyright (C) 2015 <Binh-Minh.Bui-Xuan@ens-lyon.org>.
 * GPL version>=3 <http://www.gnu.org/licenses/>.
 * $Id: engine/Engine.java 2015-03-11 buixuan.
 * ******************************************************/
package engine;

import tools.Calculs;
import tools.HardCodedParameters;
import tools.User;
import tools.User.INTERFACE;
import tools.User.WEAPON;
import tools.Position;
import tools.Sound;

import specifications.EngineService;
import specifications.EngineViewerService;
import specifications.AlgorithmService;
import specifications.DataService;
import specifications.RequireDataService;

import java.util.Timer;
import java.util.TimerTask;

import algorithm.GameAlgo;
import data.Bonus;
import data.Boss;
import data.Bullet;
import data.Deadbody;
import data.Enemy;
import data.Flame;
import data.Hitmarker;
import data.Laser;
import data.Loot;
import data.Weapon;

import java.util.Random;
import java.util.ArrayList;
import java.util.List;

public class Engine implements EngineService, RequireDataService, EngineViewerService {
	private static final double friction = HardCodedParameters.friction;
	private Timer engineClock;
	private DataService data;
	private Random gen;
	private boolean moveLeft, moveRight, moveUp, moveDown;
	private double heroesVX, heroesVY;

	private boolean isPaused;
	private Position mousPos;
	private Random rand = new Random();
	private int timeBlock = 0;

	private User.INTERFACE gameInterface = User.INTERFACE.MAIN_MENU;
	private Random rn = new Random();
	private int previousLevel = 0;
	private AlgorithmService randomAi;

	public Engine() {
	}

	@Override
	public void bindDataService(DataService service) {
		data = service;
	}

	@Override
	public void init() {
		engineClock = new Timer();
		gen = new Random();
		moveLeft = false;
		moveRight = false;
		moveUp = false;
		moveDown = false;
		isPaused = false;
		heroesVX = 0;
		heroesVY = 0;
		timeBlock = 0;
		previousLevel = 0;
		gameInterface = User.INTERFACE.MAIN_MENU;
	}

	public int getPreviousLevel() {
		return previousLevel;
	}

	public void setPreviousLevel(int previousLevel) {
		this.previousLevel = previousLevel;
	}

	@Override
	public void start() {
		engineClock.schedule(new TimerTask() {
			public void run() {

				if (gameInterface != User.INTERFACE.GAME) {
					return;
				}

				if (isPaused()) {
					return;
				}

				System.out.println("Timer " + timeBlock);

				try {
					if(timeBlock == 30) {
						randomAi.generateBoss();
					}
					updateSpeedHeroes();
					updateCommandHeroes();
					updatePositionHeroes();
					updateHeroStatus();
					updateEnemy();
					updateBoss();
					updateLaserAndEnemy();
					updateBullets();
					updateTemporaryStaticObject();
					randomAi.generateEnemy();
					updateGameStatus();
				} catch (java.util.ConcurrentModificationException e) {

				}

			}
		}, 0, HardCodedParameters.enginePaceMillis);
	}

	public void updateBoss() {

		// on deplace le boss
		for (Boss b : data.getBoss()) {
			// deplacement du boss
			double deltaX = Calculs.getDelta(data.getHero().getPosition().x, b.getPosition().x);
			double deltaY = Calculs.getDelta(data.getHero().getPosition().y, b.getPosition().y);
			b.setPosition(
					new Position(b.getPosition().x + b.getSpeed() * deltaX, b.getPosition().y + b.getSpeed() * deltaY));
			b.setLookAt(Calculs.updateLookAt(b.getPosition(), data.getHeroesPosition()));

			// tir de la balle
			if (b.timeDamage < 0) {
				// le boss tir
				Flame f = new Flame();
				f.setLifetime(50);
				f.setDamage(20);
				f.setPosition(new Position(b.getPosition().x, b.getPosition().y));
				f.setAngle(Calculs.updateLookAt(f.getPosition(), data.getHero().getPosition()));
				f.setDeltaX(10 * deltaX / Math.sqrt(deltaX * deltaX + deltaY * deltaY));
				f.setDeltaY(10 * deltaY / Math.sqrt(deltaX * deltaX + deltaY * deltaY));
				b.getFlame().add(f);
				b.setTimeDamage(rand.nextInt(10));
				data.getSoundEffect().add(Sound.SOUND.FLAME);
			} else {
				// le boss ne peut pas encore tirer a nouveau
				b.setTimeDamage(b.getTimeDamage() - 1);
			}

			// deplacement des bullets du boss

			b.setFlame(updateFlame(b.getFlame()));
		}
	}

	public ArrayList<Flame> updateFlame(ArrayList<Flame> flames) {
		ArrayList<Flame> newflames = new ArrayList<Flame>();

		for (Flame flame : flames) {
			if (flame.getLifetime() > 0) {
				flame.setLifetime(flame.getLifetime() - 1);
			} else {
				continue;
			}
			flame.setPosition(
					new Position(flame.getPosition().x + flame.getDeltaX(), flame.getPosition().y + flame.getDeltaY()));

			// si la flame a deja touche le hero on ne la supprime pas
			if (flame.getDamage() <= 0) {
				newflames.add(flame);
				continue;
			}

			if (flame.getPosition().x > 0 && flame.getPosition().x < HardCodedParameters.defaultWidth
					&& flame.getPosition().y > 0 && flame.getPosition().y < HardCodedParameters.defaultHeight) {
				// on verifie si la flame touche le hero

				if (Calculs.rectangleTouchRectangle(flame.getPosition(), HardCodedParameters.flameHeight,
						HardCodedParameters.flameHeight, data.getHeroesPosition(), HardCodedParameters.heroesWidth,
						HardCodedParameters.heroesHeight)) {
					// si c'est le cas on supprime les damage de la flame
					// (sans supprimer la flamme)
					// et on reduit les pts de vie du hero
					data.getHero().setHealth(data.getHero().getHealth() - flame.getDamage());
					data.getSoundEffect().add(Sound.SOUND.HeroesGotHit);
					flame.setDamage(0);
				}

				newflames.add(flame);
			}

		}
		return newflames;
	}

	public void updateEnemy() {
		int i = 0;
		for (Enemy p : data.getEnemys()) {
			enemyMoveToHero(p, data.getEnemys(), i);
			p.setLookAt(Calculs.updateLookAt(p.getPosition(), data.getHero().getPosition()));
			enemyGiveDamage(p);
			i++;
		}
	}

	public void updateBullets() {
		ArrayList<Bullet> bullets = new ArrayList<Bullet>();
		for (Bullet b : data.getBullets()) {
			moveBullet(b);

			if (collisionBossBullet(b, data.getBoss())) {
				// ajout score
				data.getGame().setScore(data.getGame().getScore() + 10);
				continue;
			}

			if (collisionEnemyBullet(b, data.getEnemys())) {
				// ajout score
				data.getGame().setScore(data.getGame().getScore() + 10);
				continue;
			}

			// si on sort de la zone on supprime
			if (b.getPosition().x > 0 && b.getPosition().x < HardCodedParameters.defaultWidth && b.getPosition().y > 0
					&& b.getPosition().y < HardCodedParameters.defaultHeight) {

				bullets.add(b);
			}
		}
		data.setBullet(bullets);
	}

	public void updateGameStatus() {
		// MAJ de l'interface de jeu si la vie <= 0
		if (data.getHero().getHealth() <= 0) {
			gameInterface = User.INTERFACE.SCORE;
			data.getSoundEffect().add(Sound.SOUND.GAMEOVER);
		}

		// MAJ des niveaux
		if (data.getScore() < 50) {
			data.getGame().setCurrentLevel(1);
		} else if (data.getScore() < 200) {
			data.getGame().setCurrentLevel(2);
		} else if (data.getScore() < 350) {
			data.getGame().setCurrentLevel(3);
		} else if (data.getScore() < 600) {
			data.getGame().setCurrentLevel(4);
		} else if (data.getScore() < 900) {
			data.getGame().setCurrentLevel(5);
		} else {
			if (data.getScore() % 1000 == 0) {
				data.getGame().setCurrentLevel(5 + data.getScore() / 1000);
			}
		}

		data.getGame().setTime(timeBlock);
		data.getStats().updateTime(timeBlock);
		timeBlock++;
	}

	public void enemyGiveDamage(Enemy e) {
		// si l'enemy a deja attaque, il ne peut pas reattaquer a nouveau
		// tant que le timeDamage > 0
		if (e.timeDamage > 0) {
			e.timeDamage--;
			return;
		}

		// on verifie si l'enemy est en collision avec le hero
		if (Calculs.rectangleTouchRectangle(e.getPosition(), e.getWidth() / 2, e.getHeight() / 2,
				data.getHeroesPosition(), data.getHero().getWidth(), data.getHero().getHeight())) {
			e.timeDamage = 10;
			int damage = e.getHitDamage();
			if (data.getHero().getArmor() > 0) {
				if (data.getHero().getArmor() - damage > 0)
					data.getHero().setArmor(data.getHero().getArmor() - damage);
				else
					data.getHero().setArmor(0);

			} else {
				data.getHero().setHealth(data.getHero().getHealth() - damage);
			}

			data.getSoundEffect().add(Sound.SOUND.HeroesGotHit);
			data.getStats().addDamage(timeBlock, damage);
		}

	}

	public void lootsGiveStat(Loot l) {
		switch (l.getBonus().getName()) {
		case AK47:
			data.setDebug("AK47 ammo");
			data.getAk47().setAmmo(data.getAk47().getAmmo() + l.getBonus().getWeapon().getAmmo());
			data.getSoundEffect().add(Sound.SOUND.LOOT_PICKAMMO);
			break;
		case SHOTGUN:
			data.setDebug("Shotgun ammo");
			data.getShotgun().setAmmo(data.getShotgun().getAmmo() + l.getBonus().getWeapon().getAmmo());
			data.getSoundEffect().add(Sound.SOUND.LOOT_PICKAMMO);
			break;
		case LASER:
			data.setDebug("Laser ammo");
			data.getLaserWeapon().setAmmo(data.getLaserWeapon().getAmmo() + l.getBonus().getWeapon().getAmmo());
			data.getSoundEffect().add(Sound.SOUND.LOOT_PICKAMMO);
			break;
		case SPEED:
			data.setDebug("Speed ");
			data.getHero().setSpeed(data.getHero().getSpeed() + l.getBonus().getMoreSpeed());
			data.getHero().getBonus().add(l.getBonus().getClone());
			data.getSoundEffect().add(Sound.SOUND.LOOT_STAT);
			break;
		case DAMAGE:
			data.setDebug("Damage ");
			data.getHero().getBonus().add(l.getBonus().getClone());
			data.getSoundEffect().add(Sound.SOUND.LOOT_CHOCOLAT);
			break;
		case HEALTH:
			data.setDebug("Health ");
			if (data.getHero().getHealth() < 100) {
				if (data.getHero().getHealth() + l.getBonus().getMoreHealth() > 100) {
					data.getHero().setHealth(100);
				} else {
					data.getHero().setHealth(data.getHero().getHealth() + l.getBonus().getMoreHealth());
				}
			}

			data.getSoundEffect().add(Sound.SOUND.LOOT_STAT);

			break;
		case ARMOR:
			data.setDebug("Armor ");
			if (data.getHero().getArmor() < 30) {
				if (data.getHero().getArmor() + l.getBonus().getMoreArmor() > 30) {
					data.getHero().setArmor(30);
				} else {
					data.getHero().setArmor(data.getHero().getArmor() + l.getBonus().getMoreArmor());
				}
			}

			data.getSoundEffect().add(Sound.SOUND.LOOT_STAT);

			break;
		}
	}

	/**
	 * Met a jour les hitmarkers: supprime les hitmarkers qui on un lifetime < 1 MAJ
	 * les deadbody MAJ les loots
	 */
	public void updateTemporaryStaticObject() {
		ArrayList<Hitmarker> newhitMarkers = new ArrayList<Hitmarker>();
		for (Hitmarker h : data.getHitmarkers()) {
			if (h.getLifetime() > 0) {
				h.setLifetime(h.getLifetime() - 1);
				newhitMarkers.add(h);
			}
		}
		data.setHitMarkers(newhitMarkers);

		ArrayList<Deadbody> newDeadBody = new ArrayList<Deadbody>();
		for (Deadbody h : data.getDeadBody()) {
			if (h.getLifetime() > 0) {
				h.setLifetime(h.getLifetime() - 1);
				newDeadBody.add(h);
			}
		}
		data.setDeadBody(newDeadBody);

		ArrayList<Loot> Loots = new ArrayList<Loot>();
		for (Loot l : data.getLoot()) {
			if (Calculs.rectangleTouchRectangle(l.getPosition(), l.getWidth() / 2, l.getHeight() / 2,
					data.getHeroesPosition(), data.getHero().getWidth(), data.getHero().getHeight())) {
				lootsGiveStat(l);

				data.getHero().setLifetimeBonus(HardCodedParameters.lifeTimeBonus);
			} else if (l.getLifeTime() > 0) {
				l.setLifeTime(l.getLifeTime() - 1);
				Loots.add(l);
			}
		}
		data.setLoot(Loots);

		ArrayList<Bonus> bonus = new ArrayList<Bonus>();

		for (Bonus b : data.getHero().getBonus()) {
			if (b.getLifetime() > 0) {
				b.setLifetime(b.getLifetime() - 1);
				bonus.add(b);
			} else {
				switch (b.getName()) {
				case SPEED:
					data.getHero().setSpeed(data.getHero().getSpeed() - b.getMoreSpeed());
					break;
				default:
					break;
				}

			}
		}
		data.getHero().setBonus(bonus);

	}

	public void updateHeroStatus() {
		if (data.getHero().timerShooting > 0) {
			data.getHero().timerShooting--;
		}
		if (data.getHero().getLifetimeBonus() > 0) {
			data.getHero().setLifetimeBonus(data.getHero().getLifetimeBonus() - 1);
		}
	}

	public boolean isBonusAvailabled(tools.Bonus.TYPE type) {
		for (Bonus b : data.getHero().getBonus()) {
			if (b.getName() == type)
				return true;
		}
		return false;
	}

	public boolean collisionBossBullet(Bullet b, List<Boss> enemys) {
		int index = 0;
		int indexSup = -1;

		for (Enemy e : enemys) {
			if (Calculs.rectangleTouchRectangle(b.getPosition(), HardCodedParameters.bulletHeight,
					HardCodedParameters.bulletHeight, e.getPosition(), e.getWidth(), e.getHeight())) {
				indexSup = index;
				break;
			}
			index++;
		}

		if (indexSup != -1) {

			Boss enemyHit = data.getBoss().get(indexSup);
			enemyHit.setHealth(enemyHit.getHealth() - b.getDamage());
			if (enemyHit.getHealth() <= 0) {
				data.getDeadBody().add(new Deadbody(b.getPosition()));
				data.getStats().addZombie(timeBlock);
				data.getBoss().remove(indexSup);
				randomAi.randomLoot(b.getPosition());
			}

			// ajout hitmarker
			data.getSoundEffect().add(Sound.SOUND.HITMARKER);
			data.addHitMarker(new Hitmarker(b.getPosition()));
			return true;
		}

		return false;
	}

	public boolean collisionEnemyBullet(Bullet b, List<Enemy> enemys) {
		int index = 0;
		int indexSup = -1;

		for (Enemy e : enemys) {
			if (Calculs.rectangleTouchRectangle(b.getPosition(), HardCodedParameters.bulletHeight,
					HardCodedParameters.bulletHeight, e.getPosition(), e.getWidth(), e.getHeight())) {
				indexSup = index;
				break;
			}
			index++;
		}

		if (indexSup != -1) {

			Enemy enemyHit = data.getEnemys().get(indexSup);
			if (isBonusAvailabled(tools.Bonus.TYPE.DAMAGE))
				enemyHit.setHealth(0);
			else
				enemyHit.setHealth(enemyHit.getHealth() - b.getDamage());
			if (enemyHit.getHealth() <= 0) {
				data.getDeadBody().add(new Deadbody(b.getPosition()));
				data.getStats().addZombie(timeBlock);
				data.getEnemys().remove(indexSup);
				if (rn.nextInt(100) < HardCodedParameters.pourcentageLoot) {
					randomAi.randomLoot(b.getPosition());
				}
			}

			// ajout hitmarker
			data.getSoundEffect().add(Sound.SOUND.HITMARKER);
			data.addHitMarker(new Hitmarker(b.getPosition()));
			return true;
		}

		return false;
	}

	public void updateLaserAndEnemy() {
		if (data.getLasers().size() == 0) {
			return;
		}

		ArrayList<Enemy> enemys = new ArrayList<Enemy>();
		ArrayList<Laser> lasers = new ArrayList<Laser>();

		for (Laser l : data.getLasers()) {
			if (l.getLifetime() > 0) {
				l.setLifetime(l.getLifetime() - 1);
				lasers.add(l);
			}

			for (Enemy e : data.getEnemys()) {

				if (Calculs.lineRect(l.getPosition().x, l.getPosition().y, l.getPositionFinal().x,
						l.getPositionFinal().y, e.getPosition().x - e.getWidth() / 2,
						e.getPosition().y - e.getHeight() / 2, e.getPosition().x + e.getWidth() / 2,
						e.getPosition().y + e.getHealth() / 2)) {
					data.getStats().addZombie(timeBlock);
					data.getStats().addDamage(timeBlock, 100);
					continue;
				}
				// si l'enemy n'a pas ete touche
				enemys.add(e);
			}
		}

		data.setLaser(lasers);
		data.setEnemys(enemys);
	}

	public void enemyMoveToHero(Enemy e, ArrayList<Enemy> enemys, int i) {
		double deltaX = Calculs.getDelta(data.getHero().getPosition().x, e.getPosition().x);
		double deltaY = Calculs.getDelta(data.getHero().getPosition().y, e.getPosition().y);
		int index = 0;
		// on verifie les collisions: on ne veut pas deplacer un enemy sur un
		// autre enemy
		for (Enemy en : enemys) {
			// on ne compare pas l'enemy qu'on verifie
			if (index == i) {
				continue;
			}
			if (Calculs.rectangleTouchRectangle(en.getPosition(), e.getWidth() / 2, e.getHeight() / 2, e.getPosition(),
					e.getWidth() / 2, e.getHeight() / 2)) {
				return;
			}
			index++;
		}
		e.setPosition(
				new Position(e.getPosition().x + e.getSpeed() * deltaX, e.getPosition().y + e.getSpeed() * deltaY));
	}

	public boolean isPaused() {
		return isPaused;
	}

	@Override
	public void stop() {
		engineClock.cancel();
	}

	@Override
	public void setHeroesCommand(User.COMMAND c) {
		if (c == User.COMMAND.LEFT)
			moveLeft = true;
		if (c == User.COMMAND.RIGHT)
			moveRight = true;
		if (c == User.COMMAND.UP)
			moveUp = true;
		if (c == User.COMMAND.DOWN)
			moveDown = true;
	}

	@Override
	public void releaseHeroesCommand(User.COMMAND c) {
		if (c == User.COMMAND.LEFT)
			moveLeft = false;
		if (c == User.COMMAND.RIGHT)
			moveRight = false;
		if (c == User.COMMAND.UP)
			moveUp = false;
		if (c == User.COMMAND.DOWN)
			moveDown = false;
	}

	private void updateSpeedHeroes() {
		heroesVX *= friction;
		heroesVY *= friction;
	}

	private void updateCommandHeroes() {
		if (moveLeft)
			heroesVX -= data.getHero().getHeroesStep() + data.getHero().getSpeed();
		if (moveRight)
			heroesVX += data.getHero().getHeroesStep() + data.getHero().getSpeed();
		if (moveUp)
			heroesVY -= data.getHero().getHeroesStep() + data.getHero().getSpeed();
		if (moveDown)
			heroesVY += data.getHero().getHeroesStep() + data.getHero().getSpeed();
	}

	private void updatePositionHeroes() {
		data.setHeroesPosition(Calculs.limitPosition(
				new Position(data.getHeroesPosition().x + heroesVX, data.getHeroesPosition().y + heroesVY),
				data.getHero().getWidth(), data.getHero().getHeight()));
	}

	@Override
	public void setMousePosition(Position pos) {
		data.setDebug("Mouse: x = " + pos.x + " y = " + pos.y);

		mousPos = pos;
		data.getHero().setLookAt(Calculs.updateLookAt(data.getHeroesPosition(), pos));
	}

	public void moveBullet(Bullet b) {
		b.setPosition(new Position(b.getPosition().x + b.getDeltaX(), b.getPosition().y + b.getDeltaY()));
	}

	@Override
	public void heroShoot() {

		if (data.getHero().timerShooting > 0) {
			return;
		}

		if (data.getHero().getCurrentWeapon().getAmmo() == 0) {
			data.getSoundEffect().add(Sound.SOUND.NOAMMO);
			return;
		}

		data.getSoundEffect().add(Sound.SOUND.SHOOT);

		data.getStats().addBullet(timeBlock);

		Position pos = new Position(data.getHeroesPosition().x, data.getHeroesPosition().y);

		if (data.getHero().currentWeapon != User.WEAPON.GLOCK && data.getHero().getCurrentWeapon().getAmmo() > 0) {
			data.getHero().getCurrentWeapon().setAmmo(data.getHero().getCurrentWeapon().getAmmo() - 1);
		}

		data.getHero().timerShooting = data.getHero().getCurrentWeapon().getReloadingTime();

		double deltaX = Calculs.getDelta(mousPos.x, data.getHeroesPosition().x);
		double deltaY = Calculs.getDelta(mousPos.y, data.getHeroesPosition().y);
		if (data.getHero().currentWeapon == User.WEAPON.LASER) {
			Laser laser = new Laser();
			laser.setDamage(data.getHero().getCurrentWeapon().getBulletDamage());
			laser.setPosition(pos);
			laser.setDeltaX(deltaX);
			laser.setDeltaY(deltaY);
			laser.setPositionFinal(
					new Position(laser.getPosition().x + HardCodedParameters.defaultWidth * laser.getDeltaX(),
							laser.getPosition().y + HardCodedParameters.defaultHeight * laser.getDeltaY()));
			data.addLaser(laser);
		} else {
			double distance = Math.sqrt(deltaX * deltaX + deltaY * deltaY);
			Bullet bullet = new Bullet();
			bullet.setDamage(data.getHero().getCurrentWeapon().getBulletDamage());
			bullet.setPosition(pos);
			bullet.setDeltaX(data.getHero().getCurrentWeapon().getBulletSpeed() * deltaX / distance);
			bullet.setDeltaY(data.getHero().getCurrentWeapon().getBulletSpeed() * deltaY / distance);
			data.addBullet(bullet);
		}
	}

	@Override
	public void togglePause() {
		isPaused = !isPaused;
	}

	@Override
	public void toggleDebug() {
		data.setIsDebug(!data.getIsDebug());
	}

	@Override
	public void setWeapon(WEAPON weapon) {
		data.getHero().currentWeapon = weapon;
		data.getHero().timerShooting = 0;
	}

	@Override
	public void setGameInterface(INTERFACE i) {
		this.gameInterface = i;
	}

	@Override
	public INTERFACE getGameInterface() {
		return this.gameInterface;
	}

	@Override
	public void userClickQuit() {
		System.exit(0);
	}

	@Override
	public void userClickPlaySettings() {
		this.gameInterface = User.INTERFACE.GAME_SETTINGS;
		this.data.init();
		data.getSoundEffect().add(Sound.SOUND.Click);
	}

	@Override
	public void userClickPlay() {
		data.initGame(data.getGame().getMap(), data.getGame().getDifficulty(), true);
		timeBlock = 0;
		this.gameInterface = User.INTERFACE.GAME;
	}

	@Override
	public void userClickScore() {
		System.out.println("click score");
	}

	public void bindRandomAiService(AlgorithmService randomAi) {
		this.randomAi = randomAi;
	}

}
