/* ******************************************************
 * Project alpha - Composants logiciels 2015.
 * Copyright (C) 2015 <Binh-Minh.Bui-Xuan@ens-lyon.org>.
 * GPL version>=3 <http://www.gnu.org/licenses/>.
 * $Id: tools/HardCodedParameters.java 2015-03-11 buixuan.
 * ******************************************************/
package tools;

public class HardCodedParameters {
	// ---HARD-CODED-PARAMETERS---//
	public static String defaultParamFileName = "in.parameters";
	public static double glockBulletSpeed = 80, ak47BulletSpeed = 85, shotgunBulletSpeed = 80;
	public static int glockDamage = 34, ak47Damage = 51, shotgunDamage = 95;
	public static int glockAmmo= -1, ak47Ammo = 250, shotgunAmmo = 200, laserAmmo = 0;
	public static int heroDefaultArmor = 30, heroDefaultHealth = 100, lifeTimeBonus = 3;
	public static int limitPositionBottom = 120;
	public static final int defaultWidth = 1380, defaultHeight = 735, weaponWidth = 80, weapontHeight = 80, heroesStartX = 80, heroesStartY = 200,
			heroesWidth = 60, heroesHeight = 60, heroesStep = 10, phantomWidth = 30, phantomHeight = 30, flameHeight = 50,
			phantomStep = 10, bulletHeight = 5, timerShooting = 1, minZombie = 4, pourcentageLoot = 33, reloadingTimeBOSS = 0;
	public static final int reloadingTimeAK47 = 1, reloadingTimeSHOTGUN = 10, reloadingTimeGLOCK = 6, reloadingTimeLASER = 20;
	public static final int hitMarkerSize = 45;
	public static final int enginePaceMillis = 100, spriteSlowDownRate = 7;
	public static final double friction = 0.50;
	public static final double resolutionShrinkFactor = 0.95, userBarShrinkFactor = 0.25, menuBarShrinkFactor = 0.5,
			logBarShrinkFactor = 0.15, logBarCharacterShrinkFactor = 0.1175,
			logBarCharacterShrinkControlFactor = 0.01275, menuBarCharacterShrinkFactor = 0.175;
	public static final int displayZoneXStep = 5, displayZoneYStep = 5, displayZoneXZoomStep = 5,
			displayZoneYZoomStep = 5;
	public static final double displayZoneAlphaZoomStep = 0.98;

	// ---MISCELLANOUS---//
	public static final Object loadingLock = new Object();
	public static final String greetingsZoneId = String.valueOf(0xED1C7E), simulatorZoneId = String.valueOf(0x51E77E);

	public static <T> T instantiate(final String className, final Class<T> type) {
		try {
			return type.cast(Class.forName(className).newInstance());
		} catch (final InstantiationException e) {
			throw new IllegalStateException(e);
		} catch (final IllegalAccessException e) {
			throw new IllegalStateException(e);
		} catch (final ClassNotFoundException e) {
			throw new IllegalStateException(e);
		}
	}
}
