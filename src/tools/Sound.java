/* ******************************************************
 * Project alpha - Composants logiciels 2015.
 * Copyright (C) 2015 <Binh-Minh.Bui-Xuan@ens-lyon.org>.
 * GPL version>=3 <http://www.gnu.org/licenses/>.
 * $Id: tools/Sound.java 2015-03-11 buixuan.
 * ******************************************************/
package tools;

import java.util.HashSet;
import java.util.Set;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class Sound {
	private static Set<MediaPlayer> activeSound = new HashSet<MediaPlayer>();
	
	public static enum SOUND {
		PhantomDestroyed, HeroesGotHit, None, NOAMMO, HITMARKER, Click, SHOOT, FLAME, BOSS, LOOT_STAT, LOOT_PICKAMMO, LOOT_CHOCOLAT, GAMEOVER
	};
	
	public static void playSound(String fileName, double volume, int lifetime) {
		MediaPlayer sound = new MediaPlayer(new Media(fileName));
		sound.setOnEndOfMedia(new Runnable() {

			@Override
			public void run() {
				Sound.activeSound.remove(sound);
			}
		});
		Sound.activeSound.add(sound);
		sound.setCycleCount(lifetime);
		sound.setVolume(volume);
		sound.play();
	}
}
