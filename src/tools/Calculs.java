package tools;

public class Calculs {
	public static boolean lineLine(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4) {
		double uA = ((x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3)) / ((y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1));
		double uB = ((x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3)) / ((y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1));

		if (uA >= 0 && uA <= 1 && uB >= 0 && uB <= 1) {
			return true;
		}

		return false;
	}

	public static boolean lineRect(double x1, double y1, double x2, double y2, double rx, double ry, double rw, double rh) {
		boolean left = lineLine(x1, y1, x2, y2, rx, ry, rx, ry + rh);
		boolean right = lineLine(x1, y1, x2, y2, rx + rw, ry, rx + rw, ry + rh);
		boolean top = lineLine(x1, y1, x2, y2, rx, ry, rx + rw, ry);
		boolean bottom = lineLine(x1, y1, x2, y2, rx, ry + rh, rx + rw, ry + rh);

		if (left || right || top || bottom) {
			return true;
		}
		return false;
	}
	
	public static boolean rectangleTouchRectangle(Position p1, double p1Width, double p1Height, Position p2, double p2Width,
			double p2Height) {
		double p1XA1 = p2.x - p2Width / 2;
		double p1XB1 = p2.x + p2Width / 2;
		double p1YA1 = p2.y - p2Height / 2;
		double p1YB1 = p2.y + p2Height / 2;

		double p2XA2 = p1.x - p1Width / 2;
		double p2XB2 = p1.x + p1Width / 2;
		double p2YA2 = p1.y - p1Height / 2;
		double p2YB2 = p1.y + p1Height / 2;
		return (p1XA1 < p2XB2 && p1XB1 > p2XA2 && p1YA1 < p2YB2 && p1YB1 > p2YA2);
	}
	
	public static double updateLookAt(Position a, Position target) {

		double angle = (float) Math.toDegrees(Math.atan2(target.y - a.y, target.x - a.x));

		if (angle < 0) {
			angle += 360;
		}

		return angle;
	}
	
	public static Position limitPosition(Position p, int width, int height) {

		if (p.x < width / (double) 2) {
			p.x = width / (double) 2;
		}

		if (p.y < height) {
			p.y = height;
		}

		if (p.x > HardCodedParameters.defaultWidth ) {
			p.x = HardCodedParameters.defaultWidth;
		}

		if (p.y > -HardCodedParameters.limitPositionBottom + HardCodedParameters.defaultHeight) {
			p.y = -HardCodedParameters.limitPositionBottom + HardCodedParameters.defaultHeight;
		}

		return p;
	}
	
	public static double getDelta(double a, double b) {
		return a - b;
	}

}
