/* ******************************************************
 * Project alpha - Composants logiciels 2015.
 * Copyright (C) 2015 <Binh-Minh.Bui-Xuan@ens-lyon.org>.
 * GPL version>=3 <http://www.gnu.org/licenses/>.
 * $Id: alpha/Main.java 2015-03-11 buixuan.
 * ******************************************************/
package alpha;

import tools.HardCodedParameters;
import tools.Position;
import tools.User;
import tools.Sound;
import specifications.AlgorithmService;
import specifications.DataService;
import specifications.EngineService;
import specifications.EngineViewerService;
import specifications.ViewerService;
import algorithm.GameAlgo;
import data.Data;
import engine.Engine;
import userInterface.Viewer;
//import algorithm.RandomWalker;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.event.EventHandler;
import javafx.animation.AnimationTimer;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

import javafx.scene.media.MediaPlayer;

public class Main extends Application {
	// ---HARD-CODED-PARAMETERS---//
	private static String fileName = HardCodedParameters.defaultParamFileName;

	// ---VARIABLES---//
	private static DataService data;
	private static EngineService engine;
	private static ViewerService viewer;
	private static AnimationTimer timer;
	private static AlgorithmService randomAi;
	

	// ---EXECUTABLE---//
	public static void main(String[] args) {
		// readArguments(args);

		data = new Data();
		engine = new Engine();
		viewer = new Viewer();
		randomAi = new GameAlgo();

		((Engine) engine).bindDataService(data);
		((GameAlgo) randomAi).bindEngineService(engine);
		((GameAlgo) randomAi).bindDataService(data);
		((Engine) engine).bindRandomAiService(randomAi);
		((Viewer) viewer).bindReadService(data);
		((Viewer) viewer).bindEngineViewService((EngineViewerService) engine);

		data.init();
		randomAi.init();
		engine.init();
		viewer.init();

		launch(args);
	}



	@Override
	public void start(Stage stage) {

		Sound.playSound(getHostServices().getDocumentBase() + "src/sound/zombie-horde-ambient.mp3", 0.05, MediaPlayer.INDEFINITE);
		final Scene scene = new Scene(((Viewer) viewer).getPanel());

		scene.setOnMouseMoved(evt -> viewer.setMousePosition(new Position(evt.getX(), evt.getY())));
		scene.setOnMouseClicked(evt -> viewer.userClickMouse());

		// scene.setFill(Color.WHITE);

		scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent event) {

				// si le jeu n'est pas a l'etat "GAME" alors on n'utilise pas
				// le clavier
				if (engine.getGameInterface() != User.INTERFACE.GAME) {
					return;
				}

				if (event.getCode() == KeyCode.Q) {
					engine.setHeroesCommand(User.COMMAND.LEFT);
				}
				if (event.getCode() == KeyCode.D) {
					engine.setHeroesCommand(User.COMMAND.RIGHT);
				}
				if (event.getCode() == KeyCode.Z) {
					engine.setHeroesCommand(User.COMMAND.UP);
				}
				if (event.getCode() == KeyCode.S) {
					engine.setHeroesCommand(User.COMMAND.DOWN);
				}

				if (event.getCode() == KeyCode.DIGIT1) {
					engine.setWeapon(User.WEAPON.GLOCK);
				}

				if (event.getCode() == KeyCode.DIGIT2) {
					engine.setWeapon(User.WEAPON.AK47);
				}

				if (event.getCode() == KeyCode.DIGIT3) {
					engine.setWeapon(User.WEAPON.SHOTGUN);
				}

				if (event.getCode() == KeyCode.DIGIT4) {
					engine.setWeapon(User.WEAPON.LASER);
				}

				if (event.getCode() == KeyCode.ESCAPE) {
					engine.togglePause();
				}

				if (event.getCode() == KeyCode.C) {
					engine.toggleDebug();
				}

				event.consume();
			}
		});
		scene.setOnKeyReleased(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent event) {
				if (event.getCode() == KeyCode.Q)
					engine.releaseHeroesCommand(User.COMMAND.LEFT);
				if (event.getCode() == KeyCode.D)
					engine.releaseHeroesCommand(User.COMMAND.RIGHT);
				if (event.getCode() == KeyCode.Z)
					engine.releaseHeroesCommand(User.COMMAND.UP);
				if (event.getCode() == KeyCode.S)
					engine.releaseHeroesCommand(User.COMMAND.DOWN);
				event.consume();
			}
		});
		scene.widthProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observableValue, Number oldSceneWidth,
					Number newSceneWidth) {
				viewer.setMainWindowWidth(newSceneWidth.doubleValue());
			}
		});
		scene.heightProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observableValue, Number oldSceneHeight,
					Number newSceneHeight) {
				viewer.setMainWindowHeight(newSceneHeight.doubleValue());
			}
		});

		stage.setScene(scene);
		stage.setWidth(HardCodedParameters.defaultWidth);
		stage.setHeight(HardCodedParameters.defaultHeight);
		stage.setOnShown(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				engine.start();
			}
		});
		stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				engine.stop();
			}
		});
		stage.show();

		timer = new AnimationTimer() {
			@Override
			public void handle(long l) {

				scene.setRoot(((Viewer) viewer).getPanel());

				try {

					for (Sound.SOUND s : data.getSoundEffect()) {
						switch (s) {
						case GAMEOVER:
							Sound.playSound(getHostServices().getDocumentBase() + "src/sound/game-over.mp3", 1, 1);
							break;
						case Click:
							Sound.playSound(getHostServices().getDocumentBase() + "src/sound/click-sound.wav", 1, 1);
							break;
						case HITMARKER:
							Sound.playSound(getHostServices().getDocumentBase() + "src/sound/hitmarker.mp3", 1, 1);
							break;
						case HeroesGotHit:
							Sound.playSound(getHostServices().getDocumentBase() + "src/sound/hurt.mp3", 1, 1);
							break;
						case FLAME:
							Sound.playSound(getHostServices().getDocumentBase() + "src/sound/flame.mp3", 0.4, 1);
							break;
						case BOSS:
							Sound.playSound(getHostServices().getDocumentBase() + "src/sound/boss-is-coming.mp3", 1, 1);
							break;
						case LOOT_PICKAMMO:
							Sound.playSound(getHostServices().getDocumentBase() + "src/sound/loot-ammo.mp3", 1, 1);
							break;
						case LOOT_STAT:
							Sound.playSound(getHostServices().getDocumentBase() + "src/sound/loot-stat.mp3", 1, 1);
							break;
						case LOOT_CHOCOLAT:
							Sound.playSound(getHostServices().getDocumentBase() + "src/sound/loot-chocolat.mp3", 1, 1);
							break;
						case NOAMMO:
							Sound.playSound(getHostServices().getDocumentBase() + "src/sound/OutAmmo.mp3", 1, 1);
							break;
						case SHOOT:
							switch (data.getHero().currentWeapon) {
							case AK47:
								Sound.playSound(getHostServices().getDocumentBase() + "src/sound/ak47.mp3", 1, 1);
								break;
							case SHOTGUN:
								Sound.playSound(getHostServices().getDocumentBase() + "src/sound/shotgun.mp3", 1, 1);
								break;
							case LASER:
								Sound.playSound(getHostServices().getDocumentBase() + "src/sound/laser-effect.mp3", 1, 1);
								break;
							default:
								Sound.playSound(getHostServices().getDocumentBase() + "src/sound/glock.mp3", 1, 1);
								break;
							}
							break;
						default:
							break;
						}
					}
				} catch (java.util.ConcurrentModificationException e) {

				}
				data.resetSoundEffect();
			}
		};
		timer.start();
	}

	// ---ARGUMENTS---//
	private static void readArguments(String[] args) {
		if (args.length > 0 && args[0].charAt(0) != '-') {
			System.err.println("Syntax error: use option -h for help.");
			return;
		}
		for (int i = 0; i < args.length; i++) {
			if (args[i].charAt(0) == '-') {
				if (args[i + 1].charAt(0) == '-') {
					System.err.println("Option " + args[i] + " expects an argument but received none.");
					return;
				}
				switch (args[i]) {
				case "-inFile":
					fileName = args[i + 1];
					break;
				case "-h":
					System.out.println("Options:");
					System.out.println(
							" -inFile FILENAME: (UNUSED AT THE MOMENT) set file name for input parameters. Default name is"
									+ HardCodedParameters.defaultParamFileName + ".");
					break;
				default:
					System.err.println("Unknown option " + args[i] + ".");
					return;
				}
				i++;
			}
		}
	}
}
