package data;

import tools.Position;

public class Deadbody {
	Position p;
	int lifetime = 100;
	
	public Deadbody(Position position) {
		this.p = position;
	}
	public Position getP() {
		return p;
	}
	public void setP(Position p) {
		this.p = p;
	}
	
	public int getLifetime() {
		return lifetime;
	}
	public void setLifetime(int lifetime) {
		this.lifetime = lifetime;
	}

}
