package data;

import java.util.ArrayList;
import java.util.List;

import tools.HardCodedParameters;
import tools.Position;
import tools.User;

public class Hero {
	int armor;
	int health;
	int speed;
	ArrayList<Weapon> weapons = new ArrayList<Weapon>();
	ArrayList<Bonus> bonus = new ArrayList<Bonus>();
	Position position;
	int width = HardCodedParameters.heroesWidth;
	int height = HardCodedParameters.heroesHeight;
	String status;
	double heroesVX = 0, heroesVY = 0;
	double heroesStep = HardCodedParameters.heroesStep;
	public double lookAt = 0;
	public int timerShooting = 0;
	public User.WEAPON currentWeapon = User.WEAPON.GLOCK;
	public int lifetimeBonus = 0;
	
	public int getTimerShooting() {
		return timerShooting;
	}

	public void setTimerShooting(int timerShooting) {
		this.timerShooting = timerShooting;
	}

	public int getLifetimeBonus() {
		return lifetimeBonus;
	}

	public void setLifetimeBonus(int lifetimeBonus) {
		this.lifetimeBonus = lifetimeBonus;
	}

	public void setCurrentWeapon(User.WEAPON currentWeapon) {
		this.currentWeapon = currentWeapon;
	}

	public double getLookAt() {
		return lookAt;
	}

	public void setLookAt(double lookAt) {
		this.lookAt = lookAt;
	}

	public double getHeroesStep() {
		return heroesStep;
	}

	public void setHeroesStep(double heroesStep) {
		this.heroesStep = heroesStep;
	}

	public double getHeroesVX() {
		return heroesVX;
	}

	public void setHeroesVX(double heroesVX) {
		this.heroesVX = heroesVX;
	}

	public double getHeroesVY() {
		return heroesVY;
	}

	public void setHeroesVY(double heroesVY) {
		this.heroesVY = heroesVY;
	}

	public int getArmor() {
		return armor;
	}

	public void setArmor(int armor) {
		this.armor = armor;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public List<Weapon> getWeapons() {
		return weapons;
	}

	public void setWeapons(ArrayList<Weapon> weapons) {
		this.weapons = weapons;
	}

	public Weapon getCurrentWeapon() {
		for (Weapon weapon : getWeapons()) {
			if (weapon.getName().equals(currentWeapon)) {
				return  weapon;
			}
		}
		return null;
	}

	public List<Bonus> getBonus() {
		return bonus;
	}

	public void setBonus(ArrayList<Bonus> bonus) {
		this.bonus = bonus;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
