package data;

import tools.User;

public class Weapon {
	User.WEAPON name;
	int bulletDamage;
	public int reloadingTime;
	int ammo;
	double bulletSpeed;
	public boolean unlimited;
	boolean isLaser;
	
	public Weapon() {
		this.unlimited = true;
		isLaser = false;
	}

	public int getReloadingTime() {
		return reloadingTime;
	}

	public void setReloadingTime(int reloadingTime) {
		this.reloadingTime = reloadingTime;
	}

	public boolean isUnlimited() {
		return unlimited;
	}

	public void setUnlimited(boolean unlimited) {
		this.unlimited = unlimited;
	}

	public User.WEAPON getName() {
		return name;
	}

	public void setName(User.WEAPON name) {
		this.name = name;
	}

	public int getBulletDamage() {
		return bulletDamage;
	}

	public void setBulletDamage(int bulletDamage) {
		this.bulletDamage = bulletDamage;
	}

	public int getAmmo() {
		return ammo;
	}

	public void setAmmo(int ammo) {
		this.ammo = ammo;
	}

	public double getBulletSpeed() {
		return bulletSpeed;
	}

	public void setBulletSpeed(double bulletSpeed) {
		this.bulletSpeed = bulletSpeed;
	}
}
