package data;

import tools.Position;

public class Hitmarker {
	Position p;
	int lifetime = 10;

	public Hitmarker(Position p) {
		super();
		this.p = p;
	}
	
	public Position getP() {
		return p;
	}
	public void setP(Position p) {
		this.p = p;
	}
	public int getLifetime() {
		return lifetime;
	}
	public void setLifetime(int lifetime) {
		this.lifetime = lifetime;
	}
	
	
}
