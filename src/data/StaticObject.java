package data;

import tools.Position;

public class StaticObject {
	String name;
	String background;
	Position position;
	int width;
	int height;
	boolean heroCanTravel;
	boolean enemyCanTravel;
	int lifeTime;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBackground() {
		return background;
	}

	public void setBackground(String background) {
		this.background = background;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public boolean isHeroCanTravel() {
		return heroCanTravel;
	}

	public void setHeroCanTravel(boolean heroCanTravel) {
		this.heroCanTravel = heroCanTravel;
	}

	public boolean isEnemyCanTravel() {
		return enemyCanTravel;
	}

	public void setEnemyCanTravel(boolean enemyCanTravel) {
		this.enemyCanTravel = enemyCanTravel;
	}

	public int getLifeTime() {
		return lifeTime;
	}

	public void setLifeTime(int lifeTime) {
		this.lifeTime = lifeTime;
	}
}
