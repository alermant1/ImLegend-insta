package data;

import java.util.ArrayList;
import java.util.List;

public class Map {
	ArrayList<StaticObject> staticObjects = new ArrayList<StaticObject>();
	ArrayList<Enemy> availableEnemys = new ArrayList<Enemy>();
	String mapName;
	String background;
	String backgroundMenu;
	
	public ArrayList<Enemy> getAvailableEnemys() {
		return availableEnemys;
	}

	public void setAvailableEnemys(ArrayList<Enemy> availableEnemys) {
		this.availableEnemys = availableEnemys;
	}

	public String getBackgroundMenu() {
		return backgroundMenu;
	}

	public void setBackgroundMenu(String backgroundMenu) {
		this.backgroundMenu = backgroundMenu;
	}

	public List<StaticObject> getStaticObjects() {
		return staticObjects;
	}

	public void setStaticObjects(ArrayList<StaticObject> staticObjects) {
		this.staticObjects = staticObjects;
	}

	public List<Enemy> getEnemys() {
		return availableEnemys;
	}

	public void setEnemys(ArrayList<Enemy> enemys) {
		this.availableEnemys = enemys;
	}

	public String getMapName() {
		return mapName;
	}

	public void setMapName(String mapName) {
		this.mapName = mapName;
	}

	public String getBackground() {
		return background;
	}

	public void setBackground(String background) {
		this.background = background;
	}
}
