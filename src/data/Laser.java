package data;

import tools.Position;

public class Laser extends Bullet {
	
	private Position positionFinal;
	private int lifetime;
	
	public Laser() {
		this.lifetime = 2;
	}

	public Position getPositionFinal() {
		return positionFinal;
	}

	public void setPositionFinal(Position positionFinal) {
		this.positionFinal = positionFinal;
	}

	public int getLifetime() {
		return lifetime;
	}

	public void setLifetime(int lifetime) {
		this.lifetime = lifetime;
	}

}
