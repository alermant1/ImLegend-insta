/* ******************************************************
 * Project alpha - Composants logiciels 2015.
 * Copyright (C) 2015 <Binh-Minh.Bui-Xuan@ens-lyon.org>.
 * GPL version>=3 <http://www.gnu.org/licenses/>.
 * $Id: data/Data.java 2015-03-11 buixuan.
 * ******************************************************/
package data;

import tools.HardCodedParameters;
import tools.Position;
import tools.Sound;
import tools.User;
import tools.Zombie;
import specifications.DataService;

import java.util.ArrayList;
import java.util.List;

public class Data implements DataService {
	private Hero hero;
	private Debug debug;
	private ArrayList<Sound.SOUND> sound;
	private Game game = null;
	private boolean isDebug;
	private Stats stats;
	private ArrayList<Enemy> enemys = new ArrayList<Enemy>();
	private ArrayList<Bullet> bullets = new ArrayList<Bullet>();
	private ArrayList<Laser> lasers = new ArrayList<Laser>();
	private ArrayList<Loot> loot = new ArrayList<Loot>();
	private ArrayList<Map> availableMap = new ArrayList<Map>();
	private ArrayList<Hitmarker> hitmarkers = new ArrayList<Hitmarker>();
	private ArrayList<Deadbody> deadBody = new ArrayList<Deadbody>();
	private Weapon ak47 = new Weapon();
	private Weapon shotgun = new Weapon();
	private Weapon laser = new Weapon();
	private ArrayList<Boss> boss = new ArrayList<Boss>();

	public Data() {
	}

	@Override
	public void init() {
		restartData();
		game = new Game();
		generateAvailableMap();
	}
	
	private void restartData() {
		stats = new Stats();
		debug = new Debug();
		initObjects();
		initHero();
		isDebug = false;
	}
	
	private void initObjects() {
		enemys = new ArrayList<Enemy>();
		bullets = new ArrayList<Bullet>();
		loot = new ArrayList<Loot>();
		availableMap = new ArrayList<Map>();
		hitmarkers = new ArrayList<Hitmarker>();
		deadBody = new ArrayList<Deadbody>();
		lasers = new ArrayList<Laser>();
		boss = new ArrayList<Boss>();
	}
	
	private void initHero() {
		Weapon glock = new Weapon();
		glock.bulletDamage = HardCodedParameters.glockDamage;
		glock.bulletSpeed = HardCodedParameters.glockBulletSpeed;
		glock.ammo = HardCodedParameters.glockAmmo;
		glock.reloadingTime = HardCodedParameters.reloadingTimeGLOCK;
		glock.name = User.WEAPON.GLOCK;

		ak47.ammo = HardCodedParameters.ak47Ammo;
		ak47.bulletDamage = HardCodedParameters.ak47Damage;
		ak47.reloadingTime = HardCodedParameters.reloadingTimeAK47;
		ak47.bulletSpeed = HardCodedParameters.ak47BulletSpeed;
		ak47.name = User.WEAPON.AK47;
		
		shotgun.ammo = HardCodedParameters.shotgunAmmo;
		shotgun.bulletDamage = HardCodedParameters.shotgunDamage;
		shotgun.reloadingTime = HardCodedParameters.reloadingTimeSHOTGUN;
		shotgun.bulletSpeed = HardCodedParameters.shotgunBulletSpeed;
		shotgun.name = User.WEAPON.SHOTGUN;
		
		laser.ammo = HardCodedParameters.laserAmmo;
		laser.bulletDamage = 100;
		laser.reloadingTime = HardCodedParameters.reloadingTimeLASER;
		laser.name = User.WEAPON.LASER;
		
		
		hero = new Hero();
		hero.setLifetimeBonus(0);
		hero.setPosition(new Position(HardCodedParameters.heroesStartX, HardCodedParameters.heroesStartY));
		hero.setArmor(HardCodedParameters.heroDefaultArmor);
		hero.setHealth(HardCodedParameters.heroDefaultHealth);
		hero.weapons.add(laser);
		hero.weapons.add(shotgun);
		hero.weapons.add(ak47);
		hero.weapons.add(glock);


		sound = new ArrayList<Sound.SOUND>();
	}

	private void generateAvailableMap() {
		// enemy de base
		Enemy enemy1 = new Enemy();
		enemy1.setInitHeath(100);
		enemy1.height = 50;
		enemy1.width = 50;
		enemy1.hitDamage = 5;
		enemy1.name = Zombie.TYPE.NORMAL;
		enemy1.speed = 0.02;
		
		// enemy + faster + stronger / bigger
		Enemy enemy2 = new Enemy();
		enemy2.setInitHeath(50);
		enemy2.height = 40;
		enemy2.width = 40;
		enemy2.hitDamage = 10;
		enemy2.name = Zombie.TYPE.BLUE;
		enemy2.speed = 0.03;
		
		
		// enemy + faster + stronger / bigger
		Enemy enemy3 = new Enemy();
		enemy3.setInitHeath(200);
		enemy3.height = 60;
		enemy3.width = 60;
		enemy3.hitDamage = 13;
		enemy3.name = Zombie.TYPE.BIG;
		enemy3.speed = 0.01;
		
		ArrayList<StaticObject> staticObject = new ArrayList<StaticObject>();

		// map "Hell"
		Map map = new Map();
		map.setMapName("Stairway to heaven");
		map.setBackground("backgroundMapHeaven");
		map.setBackgroundMenu("file:src/images/bkg2.png");
		map.availableEnemys.add(enemy1);
		map.availableEnemys.add(enemy2);
		map.availableEnemys.add(enemy3);
		map.staticObjects = staticObject;
		availableMap.add(map);
		
		// map "Hell"
		Map map2 = new Map();
		map2.setMapName("Highway to hell");
		map2.setBackground("backgroundMapHell");
		map2.setBackgroundMenu("file:src/images/street-background.png");
		map2.availableEnemys.add(enemy1);
		map2.availableEnemys.add(enemy2);
		map2.availableEnemys.add(enemy3);
		map2.staticObjects = staticObject;
		availableMap.add(map2);
		
	}

	@Override
	public void initGame(Map m, int difficulty, boolean debugMode) {
		Game game = new Game();
		game.score = 0;
		game.difficulty = difficulty;
		game.currentLevel = 1;
		game.debugMode = debugMode;
		game.time = 0;
		game.map = m;
		this.game = game;
		restartData();	
	}
	
	public void setIsDebug(boolean bool) {
		isDebug = bool;
	}
	
	public boolean getIsDebug( ) {
		return isDebug ;
	}


	
	public Debug getDebug() {
		return debug;
	}

	public void setDebug(String log) {
		this.debug.getLogs().add(log);
	}
	public ArrayList<Deadbody> getDeadBody() {
		return deadBody;
	}

	public void setDeadBody(ArrayList<Deadbody> deadBody) {
		this.deadBody = deadBody;
	}
	
	public ArrayList<Loot> getLoot() {
		return loot;
	}

	public void setLoot(ArrayList<Loot> newloot) {
		this.loot = newloot;
	}

	@Override
	public Position getHeroesPosition() {
		return hero.getPosition();
	}

	@Override
	public int getScore() {
		return game.getScore();
	}

	@Override
	public ArrayList<Sound.SOUND> getSoundEffect() {
		return sound;
	}

	@Override
	public void setHeroesPosition(Position p) {
		hero.setPosition(p);
	}

	@Override
	public void setStepNumber(int n) {
		game.setTime(n);
	}

	@Override
	public void addScore(int score) {
		game.setScore(game.getScore() + score);
	}

	@Override
	public void setSoundEffect(ArrayList<Sound.SOUND> s) {
		sound = s;
	}

	@Override
	public List<Bullet> getBullets() {
		return bullets;
	}

	@Override
	public ArrayList<Enemy> getEnemys() {
		return enemys;
	}

	@Override
	public Hero getHero() {
		return hero;
	}


	@Override
	public void addBullet(Bullet bullet) {
		this.bullets.add(bullet);
	}

	@Override
	public void removeBullet(int index) {
		this.bullets.remove(index);
	}


	@Override
	public void setBullet(ArrayList<Bullet> b) {
		this.bullets = b;
	}

	@Override
	public void addEnemy(Enemy e) {
		this.enemys.add(e);
	}

	@Override
	public void deleteEnemy(int i) {
		this.enemys.remove(i);
	}

	@Override
	public void setEnemys(ArrayList<Enemy> enemys) {
		this.enemys = enemys;
	}

	@Override
	public Game getGame() {
		return game;
	}

	@Override
	public ArrayList<Map> getAvailableMap() {
		return availableMap;
	}

	@Override
	public void resetSoundEffect() {
		this.sound = new ArrayList<Sound.SOUND>();
	}

	@Override
	public ArrayList<Hitmarker> getHitmarkers() {
		return this.hitmarkers;
	}

	@Override
	public void addHitMarker(Hitmarker m) {
		this.hitmarkers.add(m);
	}

	@Override
	public void removeHitMarker(int i) {
		this.hitmarkers.remove(i);
	}

	@Override
	public void setHitMarkers(ArrayList<Hitmarker> hs) {
		this.hitmarkers = hs;
	}
	
	@Override
	public Weapon getAk47() {
		return ak47;
	}

	@Override
	public Weapon getShotgun() {
		return shotgun;
	}
	
	@Override
	public Weapon getLaserWeapon() {
		return laser;
	}


	@Override
	public Stats getStats() {
		return this.stats;
	}

	@Override
	public List<Laser> getLasers() {
		return this.lasers;
	}

	@Override
	public void addLaser(Laser l) {
		this.lasers.add(l);
	}

	@Override
	public void removeLaser(int index) {
		this.lasers.remove(index);
	}

	@Override
	public void setLaser(ArrayList<Laser> l) {
		this.lasers = l;
	}

	@Override
	public ArrayList<Boss> getBoss() {
		return this.boss;
	}

	@Override
	public void addBoss(Boss b) {
		this.boss.add(b);
	}

	@Override
	public void removeBoss(int index) {
		this.boss.remove(index);
	}

	@Override
	public void bossShoot(int index, Flame b) {
		this.getBoss().get(index).getFlame().add(b);
	}

	@Override
	public void setBoss(ArrayList<Boss> boss) {
		this.boss = boss;
	}

}
