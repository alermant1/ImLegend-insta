package data;

import tools.Position;
import tools.Zombie;

public class Enemy implements specifications.Enemy{
	Zombie.TYPE name;
	int health;
	int hitDamage;
	double speed;
	Position position;
	int width;
	int height;
	double lookAt;
	public double dx;
	public double dy;
	public double timeDamage = 0;
	int initHeath;
	
	public int getInitHeath() {
		return initHeath;
	}

	public void setInitHeath(int initHeath) {
		this.health = initHeath;
		this.initHeath = initHeath;
	}

	public double getDx() {
		return dx;
	}

	public void setDx(double dx) {
		this.dx = dx;
	}

	public double getDy() {
		return dy;
	}

	public void setDy(double dy) {
		this.dy = dy;
	}

	public double getTimeDamage() {
		return timeDamage;
	}

	public void setTimeDamage(double timeDamage) {
		this.timeDamage = timeDamage;
	}

	public Zombie.TYPE getName() {
		return name;
	}

	public void setName(Zombie.TYPE name) {
		this.name = name;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public int getHitDamage() {
		return hitDamage;
	}

	public void setHitDamage(int hitDamage) {
		this.hitDamage = hitDamage;
	}

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public double getLookAt() {
		return lookAt;
	}

	public void setLookAt(double lookAt) {
		this.lookAt = lookAt;
	}
	
	public Enemy getClone() {
		Enemy newEnemy = new Enemy();
		newEnemy.setHeight(this.getHeight());
		newEnemy.setWidth(this.getWidth());
		newEnemy.setHealth(this.getHealth());
		newEnemy.setHitDamage(this.getHitDamage());
		newEnemy.setPosition(this.getPosition());
		newEnemy.setSpeed(this.getSpeed());
		newEnemy.setName(this.getName());
		newEnemy.setInitHeath(this.getInitHeath());
		return newEnemy;
	}
}
