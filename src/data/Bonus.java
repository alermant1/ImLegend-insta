package data;

public class Bonus {
	tools.Bonus.TYPE name;
	int lifeTime = 0;
	int moreHealth = 0;
	int moreSpeed = 0;
	int moreArmor = 0;
	int moreDomage = 0;
	Weapon weapon = new Weapon();

	public tools.Bonus.TYPE getName() {
		return name;
	}

	public void setName(tools.Bonus.TYPE name) {
		this.name = name;
	}
	
	public int getMoreDomage() {
		return moreDomage;
	}

	public void SetMoreDomage(int domage) {
		this.moreDomage = domage;
	}

	public int getLifetime() {
		return lifeTime;
	}

	public void setLifetime(int lifetime) {
		this.lifeTime = lifetime;
	}

	public int getMoreHealth() {
		return moreHealth;
	}

	public void setMoreHealth(int moreHealth) {
		this.moreHealth = moreHealth;
	}

	public int getMoreSpeed() {
		return moreSpeed;
	}

	public void setMoreSpeed(int moreSpeed) {
		this.moreSpeed = moreSpeed;
	}

	public int getMoreArmor() {
		return moreArmor;
	}

	public void setMoreArmor(int moreArmor) {
		this.moreArmor = moreArmor;
	}
	
	public Weapon getWeapon() {
		return weapon;
	}

	public void setWeapon(Weapon newWeapon) {
		this.weapon = newWeapon;
	}
	
	public Bonus getClone() {
		Bonus newBonus = new Bonus();
		newBonus.setMoreSpeed(this.getMoreSpeed());
		newBonus.setLifetime(this.getLifetime());
		newBonus.setMoreArmor(this.getMoreArmor());
		newBonus.setMoreHealth(this.getMoreHealth());
		newBonus.setWeapon(this.getWeapon());
		newBonus.setName(this.getName());
		
		return newBonus;
	}

}
