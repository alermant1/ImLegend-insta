package data;

import java.util.HashMap;
import java.util.Map;

public class Stats {
	private Map<Integer, Integer> zombieKilled;

	private Map<Integer, Integer> bulletShots;
	private Map<Integer, Integer> damageTaken;
	private int totalZombieKilled;
	private int totalBulletsShots;
	private int totalDamageTaken;
	public Stats() {
		init();
	}
	
	public void init() {
		this.zombieKilled = new HashMap<Integer, Integer>();
		this.bulletShots = new HashMap<Integer, Integer>();
		this.damageTaken = new HashMap<Integer, Integer>();
		this.totalZombieKilled = 0;
		this.totalBulletsShots = 0;
		this.totalDamageTaken = 0;
	}
	
	public void updateTime(int time) {
		int range = (int)(time/10);
		if (!bulletShots.containsKey(range)) {
			bulletShots.put(range, 0);
		}
		if (!zombieKilled.containsKey(range)) {
			zombieKilled.put(range, 0);
		}
		if (!damageTaken.containsKey(range)) {
			damageTaken.put(range, 0);
		}
	}
	
	public void addBullet(int time) {
		int range = (int)(time/10);
		// si la key existe deja => on incremente
		if (bulletShots.containsKey(range)) {
			bulletShots.put(range, bulletShots.get(range) + 1);
		} else {
			// sinon on initialise
			bulletShots.put(range, 1);
		}
		this.totalBulletsShots++;
	}
	
	public void addZombie(int time) {
		int range = (int)(time/10);
		// si la key existe deja => on incremente
		if (zombieKilled.containsKey(range)) {
			zombieKilled.put(range, zombieKilled.get(range) + 1);
		} else {
			// sinon on initialise
			zombieKilled.put(range, 1);
		}
		this.totalZombieKilled++;
	}
	
	public void addDamage(int time, int damage) {
		int range = (int)(time/10);
		// si la key existe deja => on incremente
		if (damageTaken.containsKey(range)) {
			damageTaken.put(range, damageTaken.get(range) + damage);
		} else {
			// sinon on initialise
			damageTaken.put(range, damage);
		}
		this.totalDamageTaken += damage;
	}

	public Map<Integer, Integer> getZombieKilled() {
		return zombieKilled;
	}

	public Map<Integer, Integer> getBulletShots() {
		return bulletShots;
	}

	public Map<Integer, Integer> getDamageTaken() {
		return damageTaken;
	}

	public int getTotalZombieKilled() {
		return totalZombieKilled;
	}

	public int getTotalBulletsShots() {
		return totalBulletsShots;
	}

	public int getTotalDamageTaken() {
		return totalDamageTaken;
	}

}
