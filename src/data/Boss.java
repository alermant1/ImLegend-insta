package data;

import java.util.ArrayList;

import tools.HardCodedParameters;

public class Boss extends Enemy{

	private int reloadingTime;
	private ArrayList<Flame> flame;
	
	public Boss() {
		this.flame = new ArrayList<Flame>();
		this.setTimeDamage(HardCodedParameters.reloadingTimeBOSS);
	}
	
	public ArrayList<Flame> getFlame() {
		return flame;
	}

	public void setBullets(ArrayList<Flame> flame) {
		this.flame = flame;
	}

	public int getReloadingTime() {
		return reloadingTime;
	}

	public void setReloadingTime(int reloadingTime) {
		this.reloadingTime = reloadingTime;
	}

	public void setFlame(ArrayList<Flame> flame) {
		this.flame = flame;
	}
}
